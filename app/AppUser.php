<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AppUser extends Model
{
    use SoftDeletes;
    //protected $appends = ['profile_image'];
    protected $fillable = ['name','password','district','is_agent','is_membership','phone_no','profile_image', 'status','points','life','help','page_qty','refer_points','refer_code','point_setting_id','app_token'];
    protected $hidden = ['pivot'];

    public function coins()
    {
        return $this->hasMany(Coin::class,'app_user_id');
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class,'app_user_id');
    }

    public function puzzles()
    {
        return $this->belongsToMany(Puzzle::class, 'app_user_puzzle', 'app_user_id', 'puzzle_id');
    }

    public function puzzleSummery()
    {
        return $this->hasMany(AppUserPuzzleSummery::class,'app_user_id');
    }

    public function getProfileImageAttribute($value)
    {
        return !empty($value) ? "http://quizpark.online/$value" : '';
    }

    public function pointCategory()
    {
        return $this->belongsTo(PointSetting::class,'point_setting_id','id');
    }

    public function referHistories()
    {
        return $this->hasMany(ReferHistory::class, 'user_id', 'id');
    }

    public function getUserLevel()
    {
        return $this->hasOne(UserLevel::class, 'user_id');
    }

    public function couponTransaction()
    {
        return $this->hasMany(CouponShop::class, 'user_id', 'id');
    }

    public function interestedParticipateLive()
    {
        return $this->belongsToMany(Live::class, 'app_user_interested_live', 'app_user_id', 'live_id');
    }

}
