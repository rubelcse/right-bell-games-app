<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 3/15/2018
 * Time: 3:50 PM
 */

namespace App\Repositories;


use App\Live;
use App\LiveCurrentQuestion;
use App\LiveQuestion;
use App\LiveScore;
use App\QuestionStage;
use App\SpinTopic;
use App\Stage;
use Carbon\Carbon;
use Illuminate\Http\Request;

class LiveRepo
{
    public function CheckLive()
    {
        $live = Live::where('from', '>=', Carbon::now()->toDateTimeString())->first();
        return $live;
    }

    public function currentLiveInfo()
    {
        $live = Live::where('from', '<=', Carbon::now()->toDateTimeString())
            ->where('to', '>=', Carbon::now()->toDateTimeString())
            ->first(['id','from','to','prize']);
        return $live;
    }

    public function checkStageQuestionLimit($live_id, $stage_id)
    {
        $stage = QuestionStage::find($stage_id);
        $question_count = LiveQuestion::where(['live_id' => $live_id, 'stage_id' => $stage_id])->count();
        return $stage->take_question_limit <= $question_count ? true : false;
    }

    public function spinQuestions()
    {
        $checkTodayQuestion = LiveCurrentQuestion::whereDate('created_at', '=', Carbon::today())->first();
        //dd($checkTodayQuestion);
        if (empty($checkTodayQuestion)) {
            //first delete all previouse data
            LiveCurrentQuestion::truncate();
            //insert data
            $topics = SpinTopic::whereStatus(1)->get();
            foreach ($topics as $topic) {
                $questions = $topic->getTopicsWiseQuestion->map->only(['id', 'question','spin_topic_id', 'options']);
                foreach ($questions as $question) {
                    //dd($question['spin_topic_id']);

                    $data['id'] = $question['id'];
                    $data['spin_topic_id'] = $question['spin_topic_id'];
                    $data['question'] = $question['question'];
                    $data['created_at'] = Carbon::now();
                    $data['updated_at'] = Carbon::now();
                    LiveCurrentQuestion::create($data);
                }
            }
        }

        $questions = LiveCurrentQuestion::has('options', '>=', 2)->with(['options' => function ($q) {
            $q->select('id', 'question_id', 'option', 'answer');
        }])->orderBy('id')->get(['id', 'question']);

      return $questions;
    }

    public function userHasAlreadyJoin($user_id, $live_id)
    {
        $liveScore = LiveScore::where(['user_id' => $user_id, 'live_id' => $live_id])->first();
        return !empty($liveScore) ? true : false;
    }

}