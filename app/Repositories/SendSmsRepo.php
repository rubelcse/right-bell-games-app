<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 3/15/2018
 * Time: 3:50 PM
 */

namespace App\Repositories;


use Illuminate\Http\Request;

class SendSmsRepo
{
    const API_TOKEN = "QUIZ_PARK-9c6948a8-7b74-4095-90ec-d4294d83b6b5"; //put ssl provided api_token here
    const SID = "QUIZNONBRANDOTP"; // put ssl provided sid here
    const DOMAIN = "https://smsplus.sslwireless.com"; //api domain // example http://smsplus.sslwireless.com

    public function singleSms($msisdn, $messageBody, $csmsId)
    {
        $params = [
            "api_token" => self::API_TOKEN,
            "sid" => self::SID,
            "msisdn" => $msisdn,
            "sms" => $messageBody,
            "csms_id" => $csmsId
        ];
        $url = trim(self::DOMAIN, '/') . "/api/v3/send-sms";
        $params = json_encode($params);
        //dd($params);

        $response =  $this->callApi($url, $params);
        return $response;
    }

    public function callApi($url, $params)
    {
        $ch = curl_init(); // Initialize cURL
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($params),
            'accept:application/json'
        ));

        $response = curl_exec($ch);

        curl_close($ch);

        return $response;
    }
}