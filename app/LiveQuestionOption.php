<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LiveQuestionOption extends Model
{
    protected $fillable = ['option', 'answer', 'question_id'];

    public function question()
    {
        return $this->belongsTo(LiveQuestion::class, 'question_id');
    }
}
