<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = ['membership_point_needed', 'first_level_member_min_refer','first_level_member', 'level_member_increase_by','number_of_level','created_at','updated_at'];
}
