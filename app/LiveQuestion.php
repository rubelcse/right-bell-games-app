<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LiveQuestion extends Model
{
    protected $fillable = ['live_id', 'stage_id', 'question'];

    public function live()
    {
       return $this->belongsTo(Live::class, 'live_id');
    }
    public function stage()
    {
        return $this->belongsTo(QuestionStage::class, 'stage_id');
    }

    public function options()
    {
        return $this->hasMany(LiveQuestionOption::class, 'question_id');
    }

    public function correctOptions()
    {
        return $this->hasOne(LiveQuestionOption::class, 'question_id')->where('answer',1);
    }
}
