<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PointSetting extends Model
{
    protected $fillable = ['title','from','to'];

    public static function boot()
    {
        parent::boot();

        PointSetting::observe(new \App\Observers\UserActionsObserver);
    }
}
