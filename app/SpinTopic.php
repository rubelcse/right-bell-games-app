<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class SpinTopic extends Model
{
    use SoftDeletes;

    protected $fillable = ['title', 'take_question_limit', 'status'];

    public static function boot()
    {
        parent::boot();

        Topic::observe(new \App\Observers\UserActionsObserver);
    }

    public function getTopicsWiseQuestion()
    {
        return $this->questions()->with(['options' => function ($q) {
            $q->select('id', 'question_id', 'option', 'answer');
        }])->orderBy(DB::raw('RAND()'))
            ->take($this->take_question_limit);
    }

    public function questions()
    {
        return $this->hasMany(SpinQuestion::class, 'spin_topic_id');
    }
}
