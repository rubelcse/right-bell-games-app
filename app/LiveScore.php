<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LiveScore extends Model
{
    protected $fillable = ['live_id', 'user_id', 'score'];

    public function user()
    {
        return $this->belongsTo(AppUser::class, 'user_id', 'id');
    }
}
