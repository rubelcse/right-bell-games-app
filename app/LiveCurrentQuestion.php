<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LiveCurrentQuestion extends Model
{
    protected $fillable = ['id','spin_topic_id','question'];

    public function options()
    {
        return $this->hasMany(SpinQuestionOption::class, 'question_id','id');
    }
}
