<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class QuestionStage extends Model
{
    protected $fillable = ['name', 'question_options', 'mark', 'negative_mark', 'status', 'take_question_limit'];

    public function getStageWiseQuestion()
    {
        return $this->questions()->with(['options' => function ($q) {
            $q->select('id', 'question_id', 'option', 'answer');
        }])->orderBy(DB::raw('RAND()'))
            ->take($this->take_question_limit);
    }

    public function questions()
    {
        return $this->hasMany(LiveQuestion::class, 'stage_id');
    }
}
