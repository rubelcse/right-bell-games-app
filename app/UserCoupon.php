<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCoupon extends Model
{
    protected $fillable = ['user_id', 'coupon_id', 'point', 'amount', 'is_used', 'created_at','updated_at'];

    public function user()
    {
        return $this->belongsTo(AppUser::class,'user_id', 'id');
    }

    public function coupon()
    {
        return $this->belongsTo(Coupon::class, 'coupon_id','id');
    }


}
