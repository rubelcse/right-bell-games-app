<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SpinQuestionsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'spin_topic_id'=> 'required',
            'question'=> 'required',
            'option1'=> 'required',
            'option2'=> 'required',
            'answer'=> 'required',
        ];
    }
}
