<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'membership_point_needed' => 'required|integer',
            'first_level_member_min_refer' => 'required|integer',
            'first_level_member' => 'required|integer',
            'level_member_increase_by' => 'required|integer',
            'number_of_level' => 'required|integer',
        ];
    }
}
