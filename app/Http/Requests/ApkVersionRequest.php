<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ApkVersionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "version" => "required|unique:apk_update_managers",
            "short_description" => "required",
            "is_current_version" => "required",
            "need_to_force" => "required",
        ];
    }
}
