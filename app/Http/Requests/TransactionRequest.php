<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TransactionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'txn_no' => 'required|unique:transactions',
            'txn_date' => 'date_format:Y-m-d H:i:s',
            'app_user_id' => 'required',
            'package_category_id' => 'required',
            'package_id' => 'required',
            'status' => 'required',
            'amount' => 'required',
        ];
    }
}
