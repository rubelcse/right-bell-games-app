<?php

namespace App\Http\Controllers;

use App\LiveQuestionOption;
use Illuminate\Http\Request;

class LiveQuestionOptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LiveQuestionOption  $liveQuestionOption
     * @return \Illuminate\Http\Response
     */
    public function show(LiveQuestionOption $liveQuestionOption)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LiveQuestionOption  $liveQuestionOption
     * @return \Illuminate\Http\Response
     */
    public function edit(LiveQuestionOption $liveQuestionOption)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LiveQuestionOption  $liveQuestionOption
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LiveQuestionOption $liveQuestionOption)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LiveQuestionOption  $liveQuestionOption
     * @return \Illuminate\Http\Response
     */
    public function destroy(LiveQuestionOption $liveQuestionOption)
    {
        //
    }
}
