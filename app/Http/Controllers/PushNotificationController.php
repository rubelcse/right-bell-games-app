<?php

namespace App\Http\Controllers;

use App\AppUser;
use App\Http\Requests\PushRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PushNotificationController extends Controller
{
    public function push()
    {
        return view('push.form');
    }

    public function sendPush(PushRequest $request)
    {
        /* $notification = AppUser::all();
         foreach ($notification as $key => $value) {
             $this->notification($value->app_token, $request->body);
         }*/
        $this->notification('hello', $request->body);

        return redirect()->back()->with('message', 'Send notification successfully!!');
    }

    public function notification($to, $title)
    {
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';

        $notification = [
            'title' => $title,
            'sound' => true,
        ];

        $extraNotificationData = ["message" => $title, "moredata" => 'dd'];

        $fcmNotification = [
            //'registration_ids' => $tokenList, //multple token array
            'to' => '/topics/push', //single token
            //'to' => 'cBmbvmyJTruYXI2UungiaM:APA91bH9fqbL0bDTpKQtBPNt556nv_RIbg2Rb4Yq27lVr0Y6axd5DTJyrQtLFMT0KZgw7FzCTgJoXjWYdfuIvucAVH2qA35wpB3dzNmDLmMEW0wn_zJtNZ31WfSwg5tFmehaKyShRuhW', //single token
            'notification' => $notification,
            'data' => $extraNotificationData
       ];

        $headers = [
            'Authorization:key=AAAA_n4_WyM:APA91bH-4WBjnbHxSM5mAEkg8Cr_3ULQuPa6eGxDhr1aU2JhwlWmL35jkf4C9jQ0SbgonuUxvHEeI3t0JdlaiL8a6lhTGwM_a5S5gTrZquvVgabXIBPCRAwgYaH5dT1P84OyWPsLTlAv',
            'Content-Type: application/json'
        ];


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);

        return true;
    }
}
