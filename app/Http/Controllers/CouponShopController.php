<?php

namespace App\Http\Controllers;

use App\AppUser;
use App\CouponShop;
use Illuminate\Http\Request;

class CouponShopController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin');
    }/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //dd(\request()->agent_id);
        $agent_id = request()->agent_id;
        $agents = AppUser::where('is_agent',1)->get()->pluck('name', 'id')->prepend('- select agent -', '');
        $items = CouponShop::with('user','agent','userCoupon', 'userCoupon.coupon');
        if (isset($agent_id) && !empty($agent_id))  {
            $items = $items->where('agent_id', $agent_id);
        }
        $items = $items->orderBY('is_receive','desc')->get();
        return view('coupon_shops.index', compact('items','agents','agent_id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CouponShop  $couponShop
     * @return \Illuminate\Http\Response
     */
    public function show(CouponShop $couponShop)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CouponShop  $couponShop
     * @return \Illuminate\Http\Response
     */
    public function edit(CouponShop $shop)
    {
        $shop->is_receive = 1;
        $shop->save();
        return redirect()->route('shops.index')->with('message', 'Saved successfully!!');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CouponShop  $couponShop
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CouponShop $couponShop)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CouponShop  $couponShop
     * @return \Illuminate\Http\Response
     */
    public function destroy(CouponShop $couponShop)
    {
        //
    }
}
