<?php

namespace App\Http\Controllers;

use App\Http\Requests\LiveQuestionRequest;
use App\LiveQuestion;
use App\LiveQuestionOption;
use App\QuestionStage;
use App\Repositories\LiveRepo;
use Illuminate\Http\Request;

class LiveQuestionController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions = LiveQuestion::with('live')->has('options', '>=', 2)->with('correctOptions','stage')->orderBy('id', 'desc')->get();
        return view('live_questions.index', compact('questions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $relations = [
            'stages' => QuestionStage::get()->pluck('name', 'id')->prepend('Please select a stage', ''),
        ];
        $answer_options = [
        '' => 'Select Answer',
        'option1' => 'Option #1',
        'option2' => 'Option #2',
    ];

        return view('live_questions.create', compact('answer_options')+ $relations);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LiveQuestionRequest $request)
    {
        $live = (new LiveRepo())->CheckLive();
        if (empty($live)) {
            return redirect()->back()->withErrors('Please create a live first');
        }
        //check question limit
        $stage_id = $request->stage_id;
        $live_id = $live->id;
        $crossQuestionLimit = (new LiveRepo)->checkStageQuestionLimit($live_id, $stage_id);
        if ($crossQuestionLimit) {
            return redirect()->back()->withErrors('You are cross your limit in this stage!!');
        }

        $data = $request->all();
        $data['live_id'] = $live->id;
        $question = LiveQuestion::create($data);

        foreach ($request->input() as $key => $value) {
            if(strpos($key, 'option') !== false && $value != '') {
                $status = $request->input('answer') == $key ? 1 : 0;
                LiveQuestionOption::create([
                    'question_id' => $question->id,
                    'option'      => $value,
                    'answer'     => $status
                ]);
            }
        }

        return redirect()->route('live_questions.index')->with('message', 'Saved successfully!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LiveQuestion  $liveQuestion
     * @return \Illuminate\Http\Response
     */
    public function show(LiveQuestion $liveQuestion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LiveQuestion  $liveQuestion
     * @return \Illuminate\Http\Response
     */
    public function edit(LiveQuestion $liveQuestion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LiveQuestion  $liveQuestion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LiveQuestion $liveQuestion)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LiveQuestion  $liveQuestion
     * @return \Illuminate\Http\Response
     */
    public function destroy(LiveQuestion $liveQuestion)
    {
        //
    }

    public function questionSchema(Request $request)
    {
        if ($request->ajax()) {
            $stage_id = $request->stage_id;
            $stage = QuestionStage::whereId($stage_id)->first();
            $no_of_options = $stage->question_options;
            $data = view('live_questions.question_schema',compact('no_of_options'))->render();
            return response()->json(['schema'=>$data]);
        }
    }


    /**
     * Delete all selected User at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if ($request->input('ids')) {
            $entries = LiveQuestion::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->options()->delete();
                $entry->delete();
            }
        }
    }
}
