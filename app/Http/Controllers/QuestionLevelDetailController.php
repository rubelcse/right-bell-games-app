<?php

namespace App\Http\Controllers;

use App\Http\Requests\QuestionLevelDetailRequest;
use App\QuestionLevel;
use App\QuestionLevelDetail;
use Illuminate\Http\Request;

class QuestionLevelDetailController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $levelDetails = QuestionLevelDetail::with('level')->orderBy('id','desc')->get();
        return view('question_level_details.index', compact('levelDetails'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $levels = QuestionLevel::whereDoesntHave('detail')->get()->pluck('title', 'id')->prepend('Please select', '');
        return view('question_level_details.create',compact('levels'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(QuestionLevelDetailRequest $request)
    {
        QuestionLevelDetail::create($request->all());
        return redirect()->route('questionLevelDetail.index')->with('message', 'Saved successfully!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\QuestionLevelDetail  $questionLevelDetail
     * @return \Illuminate\Http\Response
     */
    public function show(QuestionLevelDetail $questionLevelDetail)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\QuestionLevelDetail  $questionLevelDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(QuestionLevelDetail $questionLevelDetail)
    {
        $levels = QuestionLevel::get()->pluck('title', 'id')->prepend('Please select', '');
        return view('question_level_details.edit',compact('levels','questionLevelDetail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\QuestionLevelDetail  $questionLevelDetail
     * @return \Illuminate\Http\Response
     */
    public function update(QuestionLevelDetailRequest $request, QuestionLevelDetail $questionLevelDetail)
    {
        $questionLevelDetail->update($request->all());
        return redirect()->route('questionLevelDetail.index')->with('message', 'Saved successfully!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\QuestionLevelDetail  $questionLevelDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(QuestionLevelDetail $questionLevelDetail)
    {
        //
    }
}
