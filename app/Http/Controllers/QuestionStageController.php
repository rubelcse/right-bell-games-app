<?php

namespace App\Http\Controllers;

use App\QuestionStage;
use Illuminate\Http\Request;

class QuestionStageController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = QuestionStage::orderBy('id','desc')->get();
        return view('question_stage.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\QuestionStage  $questionStage
     * @return \Illuminate\Http\Response
     */
    public function show(QuestionStage $questionStage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\QuestionStage  $questionStage
     * @return \Illuminate\Http\Response
     */
    public function edit(QuestionStage $questionStage)
    {
        $status = [
            1 => 'Active',
            2 => 'In Active'
        ];
        return view('question_stage.edit', compact('questionStage', 'status'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\QuestionStage  $questionStage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, QuestionStage $questionStage)
    {
        $questionStage->update($request->except('question_options'));
        return redirect()->route('question_stages.index')->with('message', 'Saved successfully!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\QuestionStage  $questionStage
     * @return \Illuminate\Http\Response
     */
    public function destroy(QuestionStage $questionStage)
    {
        //
    }
}
