<?php

namespace App\Http\Controllers;

use App\Http\Requests\SpinTopicsRequest;
use App\Http\Requests\StoreTopicsRequest;
use App\SpinTopic;
use Illuminate\Http\Request;

class SpinTopicController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $topics = SpinTopic::all();
        return view('spin_topics.index', compact('topics'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('spin_topics.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SpinTopicsRequest $request)
    {
        SpinTopic::create($request->all());
        return redirect()->route('spinTopics.index')->with('message', 'Saved successfully!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SpinTopic  $spinTopic
     * @return \Illuminate\Http\Response
     */
    public function show(SpinTopic $spinTopic)
    {
        return view('spin_topics.show', compact('spinTopic'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SpinTopic  $spinTopic
     * @return \Illuminate\Http\Response
     */
    public function edit(SpinTopic $spinTopic)
    {
        return view('spin_topics.edit', compact('spinTopic'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SpinTopic  $spinTopic
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SpinTopic $spinTopic)
    {
        $spinTopic->update($request->all());
        return redirect()->route('spinTopics.index')->with('message', 'Saved successfully!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     *
     * @param  \App\SpinTopic  $spinTopic
     * @return \Illuminate\Http\Response
     */
    public function destroy(SpinTopic $spinTopic)
    {
        $spinTopic->forceDelete();
        return redirect()->route('spinTopics.index')->with('message', 'Delete successfully!!');
    }

    /**
     * Delete all selected Topic at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if ($request->input('ids')) {
            $entries = SpinTopic::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->forceDelete();
            }
        }
    }
}
