<?php

namespace App\Http\Controllers;

use App\Http\Requests\LiveRequest;
use App\Live;
use App\Repositories\LiveRepo;
use Illuminate\Http\Request;

class LiveController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lives = Live::orderBy('id', 'desc')->get();
        return view('lives.index', compact('lives'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('lives.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LiveRequest $request)
    {
        //check already after date live exist or not
        $checkLive = (new LiveRepo())->CheckLive();
        if (!empty($checkLive)) {
           return redirect()->back()->withErrors('Live Already Exist. Please try later.');
        }
        Live::create($request->all());
        return redirect()->route('lives.index')->with('message', 'Saved successfully!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Live  $live
     * @return \Illuminate\Http\Response
     */
    public function show(Live $live)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Live  $live
     * @return \Illuminate\Http\Response
     */
    public function edit(Live $life)
    {
        return view('lives.edit', compact('life'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Live  $live
     * @return \Illuminate\Http\Response
     */
    public function update(LiveRequest $request, Live $life)
    {
        $life->update($request->all());
        return redirect()->route('lives.index')->with('message', 'Saved successfully!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Live  $live
     * @return \Illuminate\Http\Response
     */
    public function destroy(Live $live)
    {
        //
    }

    public function result(Request $request,$id)
    {
        $live = Live::with(['scores' => function($q) {
            $q->with('user')->orderBy('score', 'desc');
        }])->find($id);
        return view('lives.results', compact('live'));

    }

    public function interestedToJoinLive(Request $request,$id)
    {
        $live = Live::with('interestedParticipates')->find($id);
        return view('lives.register_user_list', compact('live'));

    }
}
