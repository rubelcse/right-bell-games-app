<?php

namespace App\Http\Controllers;

use App\ReferHistory;
use Illuminate\Http\Request;

class ReferHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ReferHistory  $referHistory
     * @return \Illuminate\Http\Response
     */
    public function show(ReferHistory $referHistory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ReferHistory  $referHistory
     * @return \Illuminate\Http\Response
     */
    public function edit(ReferHistory $referHistory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ReferHistory  $referHistory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ReferHistory $referHistory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ReferHistory  $referHistory
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReferHistory $referHistory)
    {
        //
    }
}
