<?php

namespace App\Http\Controllers;

use App\Http\Requests\PackageCategoryRequest;
use App\Http\Requests\PackagesRequest;
use App\PackageCategory;
use App\Topic;
use Illuminate\Http\Request;

class PackageCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = PackageCategory::all();
        return view('categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(PackageCategoryRequest $request)
    {
        PackageCategory::create($request->all());
        return redirect()->route('packageCategory.index')->with('message', 'Saved successfully!!');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\PackageCategory $packageCategory
     * @return \Illuminate\Http\Response
     */
    public function show(PackageCategory $packageCategory)
    {
        return view('categories.show', compact('packageCategory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\PackageCategory $packageCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(PackageCategory $packageCategory)
    {
        return view('categories.edit', compact('packageCategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\PackageCategory $packageCategory
     * @return \Illuminate\Http\Response
     */
    public function update(PackageCategoryRequest $request, PackageCategory $packageCategory)
    {
        $packageCategory->update($request->all());
        return redirect()->route('packageCategory.index')->with('message', 'Saved successfully!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\PackageCategory $packageCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(PackageCategory $packageCategory)
    {
        $packageCategory->forceDelete();
        return redirect()->route('packageCategory.index')->with('message', 'Delete successfully!!');
    }

    /**
     * Delete all selected Topic at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if ($request->input('ids')) {
            $entries = PackageCategory::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->forceDelete();
            }
        }
    }
}
