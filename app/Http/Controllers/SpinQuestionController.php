<?php

namespace App\Http\Controllers;

use App\Http\Requests\SpinQuestionsRequest;
use App\SpinQuestion;
use App\SpinQuestionOption;
use App\SpinTopic;
use Illuminate\Http\Request;

class SpinQuestionController extends Controller
{
    /**
     * SpinQuestionController constructor.
     */
    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions = SpinQuestion::has('options', '>=', 2)->with('correctOptions','topic')->orderBy('id', 'desc')->get();
        return view('spin_questions.index', compact('questions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $relations = [
            'topics' => SpinTopic::get()->pluck('title', 'id')->prepend('Please select', ''),
        ];
        $answer_options = [
            '' => 'Select Answer',
            'option1' => 'Option #1',
            'option2' => 'Option #2',
        ];

        return view('spin_questions.create', compact('answer_options')+ $relations);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SpinQuestionsRequest $request)
    {
        $question = SpinQuestion::create($request->all());

        foreach ($request->input() as $key => $value) {
            if(strpos($key, 'option') !== false && $value != '') {
                $status = $request->input('answer') == $key ? 1 : 0;
                SpinQuestionOption::create([
                    'question_id' => $question->id,
                    'option'      => $value,
                    'answer'     => $status
                ]);
            }
        }

        return redirect()->route('spinQuestions.index')->with('message', 'Saved successfully!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SpinQuestion  $spinQuestion
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $question = SpinQuestion::with('topic','options','correctOptions')->findOrFail($id);
        return view('spin_questions.show', compact('question'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SpinQuestion  $spinQuestion
     * @return \Illuminate\Http\Response
     */
    public function edit(SpinQuestion $spinQuestion)
    {
        $relations = [
            'topics' => SpinTopic::get()->pluck('title', 'id')->prepend('Please select', ''),
        ];

        return view('spin_questions.edit', compact('spinQuestion') + $relations);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SpinQuestion  $spinQuestion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SpinQuestion $spinQuestion)
    {
        $spinQuestion->update($request->all());

        return redirect()->route('spinQuestions.index')->with('message', 'Saved successfully!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SpinQuestion  $spinQuestion
     * @return \Illuminate\Http\Response
     */
    public function destroy(SpinQuestion $spinQuestion)
    {
        $spinQuestion->options()->forceDelete();
        $spinQuestion->forceDelete();
        return redirect()->route('spinQuestions.index')->with('message', 'Delete successfully!!');
    }

    /**
     * Delete all selected Question at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if ($request->input('ids')) {
            $entries = SpinQuestion::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->options()->forceDelete();
                $entry->forceDelete();
            }
        }
    }
}
