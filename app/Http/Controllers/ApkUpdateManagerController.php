<?php

namespace App\Http\Controllers;

use App\ApkUpdateManager;
use App\Http\Requests\ApkVersionRequest;
use Illuminate\Http\Request;

class ApkUpdateManagerController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = ApkUpdateManager::orderBy('is_current_version', 'asc')->get();
        return view('apk_manager.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $conditionalValue = [
            '1' => "Yes",
            '2' => "No"
        ];
        return view('apk_manager.create', compact('conditionalValue'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ApkVersionRequest $request)
    {
        if ($request->is_current_version == 1) {
            ApkUpdateManager::query()->update(["is_current_version" =>2]);
        }
        ApkUpdateManager::create($request->all());
        return redirect()->route('apk-manage.index')->with('message', 'Saved successfully!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ApkUpdateManager  $apkUpdateManager
     * @return \Illuminate\Http\Response
     */
    public function show(ApkUpdateManager $apkUpdateManager)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ApkUpdateManager  $apkUpdateManager
     * @return \Illuminate\Http\Response
     */
    public function edit(ApkUpdateManager $apk_manage)
    {
        $conditionalValue = [
            '1' => "Yes",
            '2' => "No"
        ];
        return view('apk_manager.edit', compact('apk_manage','conditionalValue'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ApkUpdateManager  $apkUpdateManager
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ApkUpdateManager $apk_manage)
    {
        $apk_manage->update($request->all());
        return redirect()->route('apk-manage.index')->with('message', 'Saved successfully!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ApkUpdateManager  $apkUpdateManager
     * @return \Illuminate\Http\Response
     */
    public function destroy(ApkUpdateManager $apk_manage)
    {
        //
    }
}
