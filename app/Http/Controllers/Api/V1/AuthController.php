<?php

namespace App\Http\Controllers\Api\V1;

use App\ApkUpdateManager;
use App\AppUser;
use App\Coin;
use App\Http\Requests\Api\AppUserLoginRequest;
use App\Http\Requests\Api\AppUserRegistrationRequest;
use App\Http\Requests\Api\ForgotPasswordRequest;
use App\Http\Requests\Api\SendSmsRequest;
use App\Http\Requests\AppUserDetailsRequest;
use App\Http\Requests\UserProfileUpdateRequest;
use App\Repositories\SendSmsRepo;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;

class AuthController extends ApiController
{
    public function sendSms(SendSmsRequest $request)
    {
        if (!Hash::check(config('defaultValue.user_token'), $request->api_token)) {
            return response()->json(['status' => 0, 'message' => 'Not Authenticate']);
        }

        //Request Form Register User
        $phoneNo = $request->phone_no;
        $message = $request->message;
        $response = $this->checkAuthentication($phoneNo);
        $csmsId = 'RB-' . mt_rand(1000000000, 9999999999);

        if ($response == true) {
            //already register user -> go To login Screen
            $data['status'] = 0;
            $data['message'] = "Already Registered";
        } else {
            //not register user -> go to OTP screen and hit Registration API
            (new SendSmsRepo)->singleSms($phoneNo, $message, $csmsId);
            $data['status'] = 1;
            $data['message'] = "Success";
        }
        return response()->json($data, 200);
    }

    public function changePasswordSms(SendSmsRequest $request)
    {
        if (!Hash::check(config('defaultValue.user_token'), $request->api_token)) {
            return response()->json(['status' => 0, 'message' => 'Not Authenticate']);
        }

        //Request Form Register User
        $phoneNo = $request->phone_no;
        $message = $request->message;
        $response = $this->checkAuthentication($phoneNo);
        $csmsId = 'RB-' . mt_rand(1000000000, 9999999999);

        if (!$response) {
            //already register user -> go To login Screen
            $data['status'] = 0;
            $data['message'] = "You Are Not Registered";
        } else {
            //not register user -> go to OTP screen and hit Registration API
            (new SendSmsRepo)->singleSms($phoneNo, $message, $csmsId);
            $data['status'] = 1;
            $data['message'] = "Success";
        }
        return response()->json($data, 200);
    }

    public function forgotPassword(ForgotPasswordRequest $request)
    {
        $phoneNo = $request->phone_no;
        $password = $request->password;
        $user = AppUser::where('phone_no', $phoneNo)->first();
        if (!Hash::check(config('defaultValue.user_token'), $request->api_token) || empty($user)) {
            return response()->json(['status' => 0, 'message' => 'Not Authenticate']);
        }

        $user->password = bcrypt($request->password);
        $user->save();
        return response()->json(['status' => 1, 'message' => 'Success']);
    }

    public function checkAuthentication($phoneNo)
    {
        $user = AppUser::where('phone_no', $phoneNo)->first();
        return (!empty($user)) ? true : false;
    }

    public function appUserRegistration(AppUserRegistrationRequest $request)
    {
        $referCode = 'RB' . $this->generateReferCode();
        $data = $request->except('password');
        $data['status'] = 1;
        $data['total_quiz_point'] = 0;
        $data['total_puzzle_point'] = 0;
        $data['life'] = 5;
        $data['page_qty'] = 0;
        $data['refer_code'] = $referCode;
        $data['password'] = bcrypt($request->password);
        $data['is_agent'] = 2; //initially not agent
        $data['is_membership'] = 2; //initially not member
        $user = AppUser::create($data);
        $user->coins()->create([
            'types' => 3,
            'is_increase' => 1,
            'points' => 5000
        ]);

        $userInfo = AppUser::find($user->id);
        return response()->json($userInfo, 200);
    }

    public function appUserLogin(AppUserLoginRequest $request)
    {
        if (!Hash::check(config('defaultValue.user_token'), $request->api_token)) {
            return response()->json(['message' => 'Not Authenticate']);
        }
        $user = AppUser::where(['phone_no' => $request->phone_no])->first();

        if (!empty($user)) {
            if (Hash::check($request->password, $user->password)) {
                $user->app_token = $request->app_token;
                if ($user->isDirty()) {
                    $user->save();
                }
                //user_level
                $userInfo = AppUser::find($user->id);
                if (AppUser::has('getUserLevel')->find($user->id)) {
                    $user->user_level = $user->getUserLevel->level;
                } else {
                    $user->user_level = 0;
                }

                return response()->json($userInfo, 200);
            } else {
                return response()->json(['message' => 'Do Not Match Your Password'], 200);
            }

        } else {
            return response()->json(['message' => 'You Are Not Authenticate'], 200);
        }
    }

    function generateReferCode()
    {
        $number = mt_rand(100000, 999999); // better than rand()
        // call the same function if the barcode exists already
        if ($this->referCodeExists($number)) {
            $newNumber = mt_rand(100000, 999999);
            return $this->referCodeExists($newNumber);
        }

        // otherwise, it's valid and can be used
        return $number;
    }


    function referCodeExists($number)
    {
        // for instance, it might look like this in Laravel
        return AppUser::whereReferCode($number)->exists();
    }

    public function appUserDetails(AppUserDetailsRequest $request)
    {
        if (!Hash::check(config('defaultValue.user_token'), $request->api_token)) {
            return response()->json(['message' => 'Not Authenticate']);
        }
        //user_level
        $user = AppUser::find($request->app_user_id);
        $user_points = $user->points;
        //dd($user_points);
        if ($user_points < 500) {
            //check today low balance bonus
            $coin = Coin::where(['app_user_id' => $user->id, 'types' => 10])->whereDate('created_at', '=', Carbon::today()->toDateString())->first();
            if (empty($coin)) {
                $user->coins()->create([
                    'types' => 10,
                    'is_increase' => 1,
                    'points' => 2000
                ]);
                $user->is_low_balance_bonuse = 1;
            }
        } else {
            $user->is_low_balance_bonuse = 0;
        }

        if (AppUser::has('getUserLevel')->find($request->app_user_id)) {
            $user->user_level = $user->getUserLevel->level;
        } else {
            $user->user_level = 0;
        }
        //apk version info
        $apkInfo = ApkUpdateManager::where('is_current_version', 1)->first();
        if (!empty($apkInfo)) {
            $user->apk_update_info = $apkInfo;
        } else {
            $user->apk_update_info = [];
        }

        return response()->json($user, 200);
    }

    public function appUserProfileUpdate(UserProfileUpdateRequest $request)
    {
        $appUser = AppUser::find($request->app_user_id);
        if ($request->has('profile_image')) {
            if (!empty($appUser->profile_image)) {
                $image_path = public_path() . '/' . $appUser->profile_image;
                if (File::exists($image_path)) {
                    File::delete($image_path);
                }
            }
            $image = $request->profile_image;  // your base64 encoded
            $image = str_replace('data:image/png;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $imageName = $appUser->id . '_' . str_random(10) . '.' . 'png';
            //Image::make($image)->resize(60, 60)->save(public_path('uploads/users') . '/' . $imageName);
            Image::make($image)->save(public_path('uploads/users') . '/' . $imageName);
            //user data update
            $appUser->profile_image = 'uploads/users/' . $imageName;
        }
        $appUser->name = $request->name;
        $appUser->address = $request->address;
        $appUser->save();
        return response()->json($appUser, 200);
    }
}
