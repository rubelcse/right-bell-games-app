<?php

namespace App\Http\Controllers\Api\V1;

use App\AppUser;
use App\CurrentQuestion;
use App\Http\Requests\CategoryQuestionsRequest;
use App\Http\Requests\CategoryRequest;
use App\Http\Requests\QuestionGetRequest;
use App\Question;
use App\QuestionLevel;
use App\QuestionsOption;
use App\SpinQuestion;
use App\SpinTopic;
use App\Topic;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class SpinController extends Controller
{
    public function getAllQuestions(Request $request)
    {
        $user = AppUser::find($request->app_user_id);
        if (!Hash::check(config('defaultValue.user_token'), $request->api_token) || empty($user)) {
            return response()->json(['message' => 'Not Authenticate']);
        }

        //insert data
        $topics = SpinTopic::whereStatus(1)->get();
        $questions = [];
        foreach ($topics as $key => $topic) {
            $newQuestions = $topic->getTopicsWiseQuestion->map->only(['id', 'question', 'options']);
            $questions = array_merge($questions, $newQuestions->toArray());
        }

        $level = QuestionLevel::whereId($request->level_id)->with(['detail' => function ($query) {
            $query->select('id', 'question_level_id', 'section_a_no_of_question', 'section_a_no_of_point', 'section_b_no_of_question', 'section_b_no_of_point', 'section_c_no_of_question', 'section_c_no_of_point');
        }])->first(['id', 'title', 'playable_question', 'playable_time']);
        //dd($level);
        return response()->json([
            'questions' => $questions,
            'level' => $level
        ], 200);

    }
}
