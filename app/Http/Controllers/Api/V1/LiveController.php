<?php

namespace App\Http\Controllers\Api\V1;

use App\AppUser;
use App\Http\Requests\Api\InterestedToJoinLiveRequest;
use App\Http\Requests\LiveScoreStoreRequest;
use App\Level;
use App\LiveScore;
use App\QuestionStage;
use App\Repositories\LiveRepo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;

class LiveController extends Controller
{
    public function index(Request $request)
    {
        $user_id = $request->app_user_id;
        //$need_point = 3000;
        $user = AppUser::find($user_id);
        if (!Hash::check(config('defaultValue.user_token'), $request->api_token) || empty($user)) {
            return response()->json(['status' => 0, 'message' => 'Not Authenticate']);
        }

        //check live start or not
        $live = (new LiveRepo)->currentLiveInfo();
        $nextLive = (new LiveRepo)->CheckLive();
        //current live not available but next live present
        if (empty($live) && !empty($nextLive)) {
            $alreadyInterestedToJoin = $user->whereHas('interestedParticipateLive', function ($q) use($nextLive, $user_id) {
                $q->where(['live_id'=>$nextLive->id]);
            })->where('id', $user_id)->first();
            $nextLive->is_registered = !empty($alreadyInterestedToJoin) ? true : false;
            //dd($alreadyInterestedToJoin);
            return response()->json(['status' => 2, 'message' => 'Live not available at this time!!', "data" => ["live" => $nextLive]]);
        }

        //current live and next live not available
        if (empty($live) && empty($nextLive)) {
            return response()->json(['status' => 0, 'message' => 'Live not available at this time.Please try later!!']);
        }
        //current live not available
        if (empty($live)) {
            return response()->json(['status' => 0, 'message' => 'Live not available at this time.Please try later!!']);
        }

        //check user register/interested to join this live
        $interestedToJoin = $user->whereHas('interestedParticipateLive', function ($q) use($live, $user_id) {
            $q->where(['live_id'=>$live->id]);
        })->where('id', $user_id)->first();

        //current live not available
        if (empty($interestedToJoin)) {
            return response()->json(['status' => 0, 'message' => 'You have not previously registered to this Live. Please Try next live !!']);
        }

        //check this user already participate
        $checkParticipation = (new LiveRepo)->userHasAlreadyJoin($user_id, $live->id);
        if ($checkParticipation == true) {
            return response()->json(['status' => 3, 'message' => 'You are already participate this live!!']);
        }

        //check user coin and didect
       /* if ($user->points < $need_point) {
            return response()->json(['status' => 0, 'message' => 'Your points are not enough for join Live !!']);
        }

        $coinData['types'] = 9;
        $coinData['is_increase'] = 0;
        $coinData['points'] = $need_point;
        $coin = $user->coins()->create($coinData);*/

        //live available

        $questions = QuestionStage::with(['questions' => function ($q) use ($live) {
            $q->with('options')->select('id', 'live_id', 'question', 'stage_id')->where('live_id', $live->id);
        }])
            ->whereStatus(1)
            ->get(['id', 'name', 'question_options', 'mark', 'negative_mark', 'take_question_limit']);

        //2 * 3 puzzles
        $levelPuzzles = Level::with(['puzzles' => function ($q) {
            $q->first();
        }])->where(['level' => 3, 'stage_id' => 2])->first();

        //spin question
        $spinQuestions = (new LiveRepo)->spinQuestions();
        //dd($spinQuestions);
        $results = [
            'live' => $live,
            'stages' => $questions,
            'level_puzzle' => $levelPuzzles,
            'googly' => $spinQuestions
        ];
        return response()->json(['status' => 1, 'message' => 'success', "data" => $results], 200);
    }

    public function store(LiveScoreStoreRequest $request)
    {
        $user_id = $request->app_user_id;
        $user = AppUser::find($user_id);
        if (!Hash::check(config('defaultValue.user_token'), $request->api_token) || empty($user)) {
            return response()->json(['status' => 0, 'message' => 'Not Authenticate']);
        }

        //check live start or not
        $live = (new LiveRepo)->currentLiveInfo();
        //live not available
        if (empty($live)) {
            return response()->json(['status' => 0, 'message' => 'Live not available at this time.Please try later!!']);
        }

        //Check User Already participate
        $userCheck = LiveScore::where(['live_id' => $request->live_id, 'user_id' => $user_id])->first();
        if (!empty($userCheck)) {
            return response()->json(['status' => 0, 'message' => 'You are already participate this competition.Please try later!!']);
        }

        LiveScore::create([
            'live_id' => $request->live_id,
            'user_id' => $user_id,
            'score' => $request->score
        ]);
        return response()->json(['status' => 1, 'message' => 'Successfully Submitted!!']);

    }

    public function lastResults(Request $request)
    {
        $base_url = URL::to('/');
        //dd($base_url);
        $lastLive = LiveScore::orderBy('live_id', 'desc')->limit(1)->first();
        $user_id = $request->app_user_id;
        $user = AppUser::find($user_id);
        if (!Hash::check(config('defaultValue.user_token'), $request->api_token) || empty($user)) {
            return response()->json(['status' => 0, 'message' => 'Not Authenticate']);
        }
        $allScorers = DB::table('app_users as au')
            ->select('au.id','au.refer_code', 'au.name', 'au.phone_no', 'au.address', DB::raw("CONCAT('$base_url','/',au.profile_image) as profile_image"), 'ls.score', DB::raw('date(ls.created_at) as date'))
            ->join('live_scores as ls', 'au.id', '=', 'ls.user_id')
            ->where('ls.live_id', $lastLive->live_id)
            ->orderByDesc('ls.score')
            ->orderBy('ls.id')
            //->limit($request->take)
            ->get();
       // dd($queries);
        $position = $allScorers->search(function ($score) use ($user_id) {
            return $score->id == $user_id;
        });

        $own_position = $allScorers->where('id', $user_id)->first();
        if (empty($own_position)) {
            $own_position = AppUser::select('id', 'name', 'profile_image')->where('id', $user_id)->first();
            $own_position->score = 0;
            $own_position->position = 0;
        } else {
            $own_position->position = $position + 1;
        }
        // dd($own_position);
        return response()->json(['top_scorer' => $allScorers->take(10), 'own_info' => $own_position], 200);

    }

    public function interestedToJoinLive(InterestedToJoinLiveRequest $request)
    {
        if (!Hash::check(config('defaultValue.user_token'), $request->api_token)) {
            return response()->json(['status' => 0, 'message' => 'Not Authenticate']);
        }

        $live = (new LiveRepo)->CheckLive();
        if (empty($live)) {
            return response()->json(['status' => 0, 'message' => 'Live not available at this time.Please try later!!']);
        }

        $interested = $live->interestedParticipates()->sync([$request->app_user_id],false);
        if ($interested) {
            return response()->json(['status' => 1, 'message' => 'Successfully Submitted!!']);
        } else {
            return response()->json(['status' => 0, 'message' => 'Something Went Wrong!!']);
        }

    }
}
