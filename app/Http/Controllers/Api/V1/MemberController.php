<?php

namespace App\Http\Controllers\Api\V1;

use App\AppUser;
use App\ReferHistory;
use App\Setting;
use App\UserLevel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class MemberController extends Controller
{
    public function createMember(Request $request)
    {
        //First check the User token check
        if (!Hash::check(config('defaultValue.user_token'), $request->api_token)) {
            return response()->json(['message' => 'Not Authenticate']);
        }

        //second check the user refer code
        if ($request->has('refer_code') && !empty($request->refer_code)) {
            $referCode = $request->refer_code;
            $referUser = AppUser::whereReferCode($referCode)->first();

            if (empty($referUser)) {
                return response()->json(['message' => 'Invalid Refer Code !!']);
            }
        }

        //Setting data
        $setting = Setting::first();
        $number_of_level = $setting->number_of_level;
        $membership_point_needed = $setting->membership_point_needed;
        $first_level_member_min_refer = $setting->first_level_member_min_refer;
        $first_level_member_limit = $setting->first_level_member;
        $first_level_member_increase_by = $setting->level_member_increase_by;
        //dd($first_level_member_limit);

        //create member and decrease coin
        $user_id = $request->app_user_id;
        $user = AppUser::find($user_id);

        //if not available membership point
        if ($user->points < $membership_point_needed) {
            return response()->json(['status' => 0,'message' => 'Your points are not enough for membership !!']);
        }

        //If available membership point{ member + point decrease}
        //member and decrease point
        $user->is_membership = 1;
        $user->save();

        //coin history added
        $coinData['types'] = 7;
        $coinData['is_increase'] = 0;
        $coinData['points'] = $membership_point_needed;
        $coin = $user->coins()->create($coinData);

        //If Refer Code


        if (!empty($referUser)) {
            //added refer point + refer history
            $response = $this->add_refer_point_and_history($referUser, $user_id);

            if ($response == true) {
                //check user level table
                $userLevel = $referUser->getUserLevel;
                if (!empty($userLevel)) {
                    //already into user level
                    return response()->json(['message' => 'success']);
                } else {
                    //not exist into user level
                    $userRefers = ReferHistory::whereUserId($referUser->id)->get();

                    //check the minimum refer condition
                    if (count($userRefers) >= $first_level_member_min_refer) {
                        $firstLevelUsers = UserLevel::whereLevel(1)->get();
                       // dd(count($firstLevelUsers) >= $first_level_member_limit);
                        //check first level user limit
                        if (count($firstLevelUsers) >= $first_level_member_limit) {

                            //all user level increase + check maximum level user + refer point added + first level limit increase + added refer user
                            $userLevels = UserLevel::all();
                            //dd($userLevels);

                            foreach ($userLevels as $userLevel) {
                                $userLevel->level += 1;
                                //dd($userLevel->level);
                                //add user refer point
                                $user = AppUser::whereId($userLevel->user_id)->first();
                                $user->refer_points += $this->findPoint($userLevel->level);
                                $user->save();
                                //Maximum user level check
                                if ($userLevel->level > $number_of_level) {
                                    //delete level
                                    $userLevel->delete();
                                } else {
                                    //update level
                                    $userLevel->save();
                                }
                            }

                            //increase first level user limit
                            $setting->first_level_member *= $first_level_member_increase_by;
                            $setting->save();

                            //added refer user into user level table
                            $referUser->getUserLevel()->create(['level' => 1]);
                            return response()->json(['message' => 'success']);
                        } else {
                            // added refer user into user level table
                            $referUser->getUserLevel()->create(['level' => 1]);
                            return response()->json(['message' => 'success']);
                        }
                    } else {
                        return response()->json(['message' => 'success']);
                    }

                }

            } else {
                //else return and message
                return response()->json(['message' => 'Something Went Wrong into user refer !!']);
            }

        }

        return response()->json(['message' => 'success']);
    }

    public function findPoint($level)
    {
        switch ($level) {
            case '1' :
                return '0';
                break;

            case '2' :
                return '200';
                break;

            case '3' :
                return '400';
                break;

            case '4' :
                return '800';
                break;

            case '5' :
                return '1500';
                break;

            case '6' :
                return '2000';
                break;

            case '7' :
                return '4000';
                break;

            case '8' :
                return '6000';
                break;

            case '9' :
                return '10000';
                break;

        }
    }


    public function add_refer_point_and_history($referUser, $appUserId)
    {
        if (!empty($referUser)) {
            $referUser->refer_points += 50;
            $referUser->save();

            //added data into refer history table
            $referUser->referHistories()->create(['refer_by' => $appUserId]);
            return true;
        } else {
            return false;
        }
    }
}
