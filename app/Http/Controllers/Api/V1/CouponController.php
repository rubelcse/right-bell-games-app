<?php

namespace App\Http\Controllers\Api\V1;

use App\AppUser;
use App\Coin;
use App\Coupon;
use App\Http\Requests\Api\BuyCouponRequest;
use App\Http\Requests\Api\CouponTransferRequest;
use App\UserCoupon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class CouponController extends Controller
{
    public function getCoupons(Request $request)
    {
        if (!Hash::check(config('defaultValue.user_token'), $request->api_token)) {
            return response()->json(['status' => 0,'message' => 'Not Authenticate']);
        }

        $coupons = Coupon::get(['id', 'name','need_point', 'amount']);
        return response()->json($coupons, 200);
    }

    public function buyCoupons(BuyCouponRequest $request)
    {
        if (!Hash::check(config('defaultValue.user_token'), $request->api_token)) {
            return response()->json(['status' => 0,'message' => 'Not Authenticate']);
        }
        $user_id = $request->app_user_id;
        $coupon_id = $request->coupon_id;
        $appUser = AppUser::where('id', $user_id)->first();
        $coupon = Coupon::whereId($coupon_id)->first();

        if ($appUser->points < $coupon->need_point) {
            return response()->json(['status' => 0,'message' => 'Coin Unavailable']);
        }
        //Decrees Coin
        $decreesData['app_user_id'] = $appUser->id;
        $decreesData['points'] = $coupon->need_point;
        $decreesData['types'] = 8;
        $decreesData['is_increase'] = 0;
        Coin::create($decreesData);

        //user coupon history
        $userCouponData['user_id'] = $user_id;
        $userCouponData['coupon_id'] = $coupon->id;
        $userCouponData['point'] = $coupon->need_point;
        $userCouponData['amount'] = $coupon->amount;
        $coupon = UserCoupon::create($userCouponData);
        if ($coupon) {
            $status = 1;
            $message = 'success';
        } else {
            $status = 0;
            $message = 'Something went wrong';
        }

        return response()->json(['status' => $status,'message' => $message]);
    }

    public function getUserCoupons(Request $request)
    {
        if (!Hash::check(config('defaultValue.user_token'), $request->api_token)) {
            return response()->json(['status' => 0,'message' => 'Not Authenticate']);
        }

        $user_id = $request->app_user_id;

        $userCoupons = DB::table('user_coupons')
            ->select(['user_coupons.id','user_coupons.coupon_id', 'coupons.name', 'user_coupons.point','user_coupons.amount','user_coupons.is_used'])
            ->join('coupons', function ($join) {
                $join->on('user_coupons.coupon_id','=', 'coupons.id');
            })
            ->where('user_id', $user_id)
            ->orderBy('user_coupons.is_used', 'desc')
            ->get();
        return response()->json($userCoupons, 200);
    }

    public function getAgentCoupons(Request $request)
    {
        if (!Hash::check(config('defaultValue.user_token'), $request->api_token)) {
            return response()->json(['status' => 0,'message' => 'Not Authenticate']);
        }

        $user_id = $request->app_user_id;

        $userCoupons = DB::table('coupon_shops')
            ->select(['user_coupons.id','app_users.name as user_name','app_users.phone_no','user_coupons.coupon_id', 'coupons.name', 'user_coupons.point','user_coupons.amount','coupon_shops.is_receive','user_coupons.is_used'])
            ->join('user_coupons', function ($join) {
                $join->on('user_coupons.id','=', 'coupon_shops.user_coupon_id');
            })
            ->join('coupons', function ($join) {
                $join->on('user_coupons.coupon_id','=', 'coupons.id');
            })
            ->join('app_users', function ($join) {
                $join->on('coupon_shops.user_id','=', 'app_users.id');
            })
            ->where('coupon_shops.agent_id', $user_id)
            ->orderBy('coupon_shops.is_receive', 'desc')
            ->get();
        return response()->json($userCoupons, 200);
    }

    public function CouponTransfer(CouponTransferRequest $request)
    {
        $transfer_from = AppUser::find($request->app_user_id);
        if (!Hash::check(config('defaultValue.user_token'), $request->api_token) || empty($transfer_from) || !Hash::check($request->password, $transfer_from->password)) {
            return response()->json(['status' => 0,'message' => 'Not Authenticate']);
        }

        $user_coupon_id = $request->user_coupon_id;
        $user_id = $request->app_user_id;
        $refer_code = $request->refer_code;
        $agent = AppUser::where('refer_code', $refer_code)->first();
        $user = AppUser::where('id', $user_id)->first();
        $userCoupon = UserCoupon::where('id', $user_coupon_id)->first();

        //check agent
        if (empty($agent) || $agent->is_agent != 1) {
            return response()->json(['status' => 0,'message' => 'Agent Not found']);
        }

        //check coupon is used or not
        if ($userCoupon->is_used != 2) {
            return response()->json(['status' => 0,'message' => 'This coupon already used']);
        }

        //user coupon used
        $userCoupon->is_used = 1;
        $userCoupon->save();

        //coupon transaction
        $shopCoupon = $user->couponTransaction()->create([
            'agent_id' => $agent->id,
            'user_coupon_id' => $userCoupon->id,
            'is_receive' => 2,
        ]);

        if ($shopCoupon) {
            $data['status'] = 1;
            $data['message'] = 'success';
        } else {
            $data['status'] = 0;
            $data['message'] = 'Something went wrong';
        }

        return response()->json($data, 200);
    }
}
