<?php

namespace App\Http\Controllers;

use App\Http\Requests\QuestionLevelRequest;
use App\QuestionLevel;
use Illuminate\Http\Request;

class QuestionLevelController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $levels = QuestionLevel::orderBy('id','desc')->get();
        return view('question_level.index', compact('levels'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('question_level.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(QuestionLevelRequest $request)
    {
        QuestionLevel::create($request->all());
        return redirect()->route('questionLevel.index')->with('message', 'Saved successfully!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\QuestionLevel  $questionLevel
     * @return \Illuminate\Http\Response
     */
    public function show(QuestionLevel $questionLevel)
    {
        return view('question_level.show', compact('questionLevel'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\QuestionLevel  $questionLevel
     * @return \Illuminate\Http\Response
     */
    public function edit(QuestionLevel $questionLevel)
    {
        return view('question_level.edit', compact('questionLevel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\QuestionLevel  $questionLevel
     * @return \Illuminate\Http\Response
     */
    public function update(QuestionLevelRequest $request, QuestionLevel $questionLevel)
    {
        $questionLevel->update($request->all());
        return redirect()->route('questionLevel.index')->with('message', 'Saved successfully!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\QuestionLevel  $questionLevel
     * @return \Illuminate\Http\Response
     */
    public function destroy(QuestionLevel $questionLevel)
    {
        $questionLevel->forceDelete();
        return redirect()->route('questionLevel.index')->with('message', 'Delete successfully!!');
    }
}
