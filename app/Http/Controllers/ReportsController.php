<?php

namespace App\Http\Controllers;

use App\AppUser;
use App\Http\Requests\LeaderBoardRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportsController extends Controller
{
    public function generateLeaderBoard(Request $request)
    {
        return view('reports.leader_board');
    }

    public function generateLeaderBoardResult(LeaderBoardRequest $request)
    {
        $from = $request->from." "."00:00:00";
        $to = $request->to." "."23:59:59";
        $allScorers = DB::table('app_users as u')
            ->join(DB::raw("(SELECT c.app_user_id, SUM(c.points) as score FROM coins c 
                where types IN(1,2) AND c.is_increase=1 AND (c.created_at BETWEEN '$from' AND '$to') GROUP BY c.app_user_id ORDER BY SUM(c.points)) as ts"), function ($join) {
                $join->on('ts.app_user_id', '=', 'u.id');
            })
            //->select(['u.id', 'u.name', 'u.profile_image', 'ts.score']) //Select fields you need.
            ->select('u.id', 'u.refer_code', 'u.name','u.phone_no', 'ts.score', 'u.profile_image as profile_image')//Select fields you need.
            ->orderBy('ts.score', 'desc')
            ->get();

        return view('reports.leader_board', compact('allScorers','from','to'));

    }
}
