<?php

namespace App\Http\Controllers;

use App\SpinQuestionOption;
use Illuminate\Http\Request;

class SpinQuestionOptionController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions_options = SpinQuestionOption::with('question')->get();
        return view('spin_questions_options.index', compact('questions_options'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SpinQuestionOption  $spinQuestionOption
     * @return \Illuminate\Http\Response
     */
    public function show(SpinQuestionOption $spinQuestionOption)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SpinQuestionOption  $spinQuestionOption
     * @return \Illuminate\Http\Response
     */
    public function edit(SpinQuestionOption $spin_questions_option)
    {
        return view('spin_questions_options.edit', compact('spin_questions_option'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SpinQuestionOption  $spinQuestionOption
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SpinQuestionOption $spin_questions_option)
    {
        $spin_questions_option->update($request->all());
        return redirect()->route('spin_questions_options.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SpinQuestionOption  $spinQuestionOption
     * @return \Illuminate\Http\Response
     */
    public function destroy(SpinQuestionOption $spin_questions_option)
    {
        $spin_questions_option->forceDelete();
        return redirect()->route('spin_questions_options.index');
    }

    /**
     * Delete all selected QuestionsOption at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if ($request->input('ids')) {
            $entries = SpinQuestionOption::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->forceDelete();
            }
        }
    }
}
