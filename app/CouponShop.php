<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CouponShop extends Model
{
    protected $fillable = ['user_id', 'agent_id','user_coupon_id','is_receive'];

    public function user()
    {
        return $this->belongsTo(AppUser::class, 'user_id', 'id');
    }

    public function agent()
    {
        return $this->belongsTo(AppUser::class,'agent_id', 'id');
    }

    public function userCoupon()
    {
        return $this->belongsTo(UserCoupon::class, 'user_coupon_id', 'id');
    }
}
