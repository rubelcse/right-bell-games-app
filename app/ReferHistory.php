<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReferHistory extends Model
{
    protected $fillable = ['user_id', 'refer_by','created_at','updated_at'];
}
