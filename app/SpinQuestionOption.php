<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SpinQuestionOption extends Model
{
    use SoftDeletes;

    protected $fillable = ['option', 'answer', 'question_id'];

    public static function boot()
    {
        parent::boot();

        QuestionsOption::observe(new \App\Observers\UserActionsObserver);
    }

    /**
     * Set to null if empty
     * @param $input
     */

    public function question()
    {
        return $this->belongsTo(SpinQuestion::class, 'question_id');
    }
}
