<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApkUpdateManager extends Model
{
    protected $fillable = ['version','short_description','is_current_version','need_to_force'];
}
