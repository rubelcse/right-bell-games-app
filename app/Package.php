<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Package extends Model
{
    use SoftDeletes;

    protected $fillable = ['package_category_id','name','quantity','price','status'];

    public function category()
    {
        return $this->belongsTo(PackageCategory::class,'package_category_id','id');
    }
}
