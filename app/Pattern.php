<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pattern extends Model
{
    protected $fillable = ['stage_id','level','blank_pos','move_pos','status'];

    public function stage()
    {
        return $this->belongsTo(Stage::class);
    }
}
