<?php
namespace App;

use App\Observers\UserActionsObserver;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Question
 *
 * @package App
 * @property string $topic
 * @property text $question_text
 * @property text $code_snippet
 * @property text $answer_explanation
 * @property string $more_info_link
*/
class Question extends Model
{
    use SoftDeletes;

    protected $fillable = ['topic_id','question','status', 'created_by', 'updated_by'];
    //protected $with = ['correctOptions'];

    public static function boot()
    {
        parent::boot();
        Question::observe(new UserActionsObserver);
    }

    /**
     * Set to null if empty
     * @param $input
     */

    public function setTopicIdAttribute($input)
    {
        $this->attributes['topic_id'] = $input ? $input : null;
    }

    public function topic()
    {
        return $this->belongsTo(Topic::class, 'topic_id')->withTrashed();
    }

    public function options()
    {
        return $this->hasMany(QuestionsOption::class, 'question_id')->withTrashed();
    }

    public function correctOptions()
    {
        return $this->hasOne(QuestionsOption::class, 'question_id')->where('answer',1);
    }
}
