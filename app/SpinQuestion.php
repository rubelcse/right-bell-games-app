<?php

namespace App;

use App\Observers\UserActionsObserver;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SpinQuestion extends Model
{
    use SoftDeletes;

    protected $fillable = ['spin_topic_id','question','status', 'created_by', 'updated_by'];
    //protected $with = ['correctOptions'];

    public static function boot()
    {
        parent::boot();
        Question::observe(new UserActionsObserver);
    }

    /**
     * Set to null if empty
     * @param $input
     */

    public function setSpinTopicIdAttribute($input)
    {
        $this->attributes['spin_topic_id'] = $input ? $input : null;
    }

    public function topic()
    {
        return $this->belongsTo(SpinTopic::class, 'spin_topic_id');
    }

    public function options()
    {
        return $this->hasMany(SpinQuestionOption::class, 'question_id');
    }

    public function correctOptions()
    {
        return $this->hasOne(SpinQuestionOption::class, 'question_id')->where('answer',1);
    }
}
