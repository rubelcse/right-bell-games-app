<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserLevel extends Model
{
    protected $fillable = ['user_id', 'level', 'created_at','updated_at'];

    public function user()
    {
        return $this->belongsTo(AppUser::class, 'user_id','id');
    }
}
