<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Live extends Model
{
    protected $fillable = ['from', 'to','prize','point_qty'];

    public function questions()
    {
        return $this->hasMany(LiveQuestion::class, 'live_id');
    }

    public function scores()
    {
        return $this->hasMany(LiveScore::class);
    }

    public function interestedParticipates()
    {
        return $this->belongsToMany(AppUser::class, 'app_user_interested_live', 'live_id', 'app_user_id');
    }


}
