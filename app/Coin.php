<?php

namespace App;

use App\Observers\CoinActionsObserver;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Coin extends Model
{
    use SoftDeletes;
    protected $fillable = ['app_user_id','types','is_increase','points'];

    public static function boot()
    {
        parent::boot();
        Coin::observe(CoinActionsObserver::class);
    }

    public function appUser()
    {
       return $this->belongsTo(AppUser::class);
    }
}
