<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDistrictIsAgentPasswordFieldIntoAppUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('app_users', function (Blueprint $table) {
            $table->string('password')->after('phone_no');
            $table->string('district')->after('address');
            $table->enum('is_agent', [1,2])->after('status')->default(2)->comment('1=agent; 2=not agent');
            $table->enum('is_membership', [1,2])->after('is_agent')->default(2)->comment('1=membership; 2=not membership');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('app_users', function (Blueprint $table) {
            $table->dropColumn('password');
            $table->dropColumn('district');
            $table->dropColumn('is_agent');
            $table->dropColumn('is_membership');
        });
    }
}
