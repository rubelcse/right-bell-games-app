<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionLevelDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_level_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('question_level_id')->unsigned()->nullable();
            $table->foreign('question_level_id')->references('id')->on('question_levels');
            $table->integer('section_a_no_of_question');
            $table->integer('section_a_no_of_point');
            $table->integer('section_b_no_of_question');
            $table->integer('section_b_no_of_point');
            $table->integer('section_c_no_of_question');
            $table->integer('section_c_no_of_point');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('question_level_details');
    }
}
