<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionStagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_stages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('question_options');
            $table->float('mark');
            $table->float('negative_mark')->nullable();
            $table->integer('take_question_limit')->nullable();
            $table->tinyInteger('status')->default(1)->comment='1=active, 2=inactive';
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->index(['deleted_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('question_stages');
    }
}
