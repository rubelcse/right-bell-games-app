<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApkUpdateManagersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apk_update_managers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('version');
            $table->string('short_description')->nullable();
            $table->tinyInteger('is_current_version')->default(1);
            $table->tinyInteger('need_to_force')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('apk_update_managers');
    }
}
