<?php

print_r($_POST); die();
function generateRandomString($length = 40)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function EncryptDataWithPublicKey($data)
{
    $pgPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAiCWvxDZZesS1g1lQfilVt8l3X5aMbXg5WOCYdG7q5C+Qevw0upm3tyYiKIwzXbqexnPNTHwRU7Ul7t8jP6nNVS/jLm35WFy6G9qRyXqMc1dHlwjpYwRNovLc12iTn1C5lCqIfiT+B/O/py1eIwNXgqQf39GDMJ3SesonowWioMJNXm3o80wscLMwjeezYGsyHcrnyYI2LnwfIMTSVN4T92Yy77SmE8xPydcdkgUaFxhK16qCGXMV3mF/VFx67LpZm8Sw3v135hxYX8wG1tCBKlL4psJF4+9vSy4W+8R5ieeqhrvRH+2MKLiKbDnewzKonFLbn2aKNrJefXYY7klaawIDAQAB";
    $public_key = "-----BEGIN PUBLIC KEY-----\n" . $pgPublicKey . "\n-----END PUBLIC KEY-----";
    $key_resource = openssl_get_publickey($public_key);
    openssl_public_encrypt($data, $crypttext, $key_resource);
    return base64_encode($crypttext);
}

function SignatureGenerate($data)
{
    $merchantPrivateKey = "MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQC3sECMILUvlRiEv8sbM9+QXRG6QSqiNxhkfnArht2eI0c97hYEQgvwzssPR38zmWGlTZ1m9V8dIYIhIF4J+vzA6wnNEIePexWmDUsY9o4ED7J/QGd60gJj8WLqFFXbz4W3YcPhfR2vyAEeRgLvSvuJUSvQn/CWZVeSofzkuOSAlT2233gSWS3laL/E9EBBBMXCe3t2Uh35WI7swUSrYe10/ObhOjdYfIDVFF2hH+5A5i/j1B1gJcpH91NaknbfWDHDCLzYhLqu7RhqTkSVfGqkhSOieLTjMte4oCGItk9GAgXetS0Q9quSoJMDJIvM4J25Iih8nzMgtQM+xxtFrI3TAgMBAAECggEAIHIJdLbySg1T317hSVTIUH+f+f44wZ5puJZ8ybNwwdYJZ27+hJIF+esNAw3l8f9NuB0ViHIe8s5QZfmJosmqi4j4mjgpObbMBCCk+yvRufXr2X5OyOr/cRfaGO6um7z0KYgjSVkIPVxeljJXolzkQUUNMDSQEoCkD0p9mQjvk6Z8JkOddHikAuS90p9/hR6p8vCNNFeaA6DK6sAw0xigLeEa/AM9c+Ll0+qtwYvT1UlwY4v4JWnQzEa7djLhwW5oErcOjCy1/XLACqFWXgAIqTWu/CHqZGGiYbW4wwRNoEm7qGJn4eh4bfLv9eSGfl6lxW0HGwaOFRaZq660OpifAQKBgQDjermhrQBwrohmPGST6LdcLsDPK5oKdAV1K/TqSuXgse3Xex26zXVdcugD4M4bAHoZWsiJZfJJDX968IODxO1DUKnB5P7fXcoFwVCe+AqU0DB/u/42T6I+tuMsZeu52u8lTg34Hi9wXTLExmBMiWx3mYcSuuPM2Vsm5Xn9YRszkQKBgQDOt/7VVRzA2fpXP2Bjh5RghNATrjermUEWUDW3ijSArRe1w+q9fcBVTO6RvaF/A59yEYeEBjAu07B3AB8M1npKPgXJKnjk0p/a/SxUxzuNFrYeHD3YImzjPQDW3du7AA17f4jyr4qseJVN9CGQDQa44AfKCOeaviLd4/8ZFlnxIwKBgA4ySId4foCJw+Vte3FnopnUKSBEvlZMq1KGrcA5cGM3d3ptKovP6rUsSKKFUxeIThZq/fzlKdu5Re1sOSjuD0iWLiH1oppaBhOZ3ugs3qwu2ff+yXymN5/mVgMdUTrGGQqg9mo3qApBJrv1DWl5YhWMtUtL8o+EORYSWCyh9MqhAoGADowcPt8Seu/cEi+/lMGBy5kNG5VzDXxnfC6A2T28nCGSPZRf/J3Q3y/eyhrSNqIGonxhUQUudt0m2fkENl8RlA/xUErQ3ymDXmHasMykgDVM86mBIKiriQJOAND0GaDTUhGTjaCU3bZOq4G2qvB/vJrncWxXACjJQ1w8I4PTKIkCgYBKuBbkP1QfiQfvLF5nM5P7Du++56XPwMjO9s2pF/xRhTI11c9blWfxdhDRK+XoG+S8ZB5nnIF9x74xHKUE2v7ZEviQj3ngBIiB9Pi6AdTAcTfnrP/wJejBi/1yy80KBqtKye4brFPFV3QcjUi5sVykRfoHOWlig2QKzK40sux8aw==";
    $private_key = "-----BEGIN RSA PRIVATE KEY-----\n" . $merchantPrivateKey . "\n-----END RSA PRIVATE KEY-----";
    openssl_sign($data, $signature, $private_key, OPENSSL_ALGO_SHA256);
    return base64_encode($signature);
}

function HttpPostMethod($PostURL, $PostData)
{
    $url = curl_init($PostURL);
    $posttoken = json_encode($PostData);
    $header = array(
        'Content-Type:application/json',
        'X-KM-Api-Version:v-0.2.0',
        'X-KM-IP-V4:' . get_client_ip(),
        'X-KM-Client-Type:PC_WEB'
    );

    curl_setopt($url, CURLOPT_HTTPHEADER, $header);
    curl_setopt($url, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($url, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($url, CURLOPT_POSTFIELDS, $posttoken);
    curl_setopt($url, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($url, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($url, CURLOPT_SSL_VERIFYPEER, 0);

    $resultdata = curl_exec($url);
    $ResultArray = json_decode($resultdata, true);
    curl_close($url);
    return $ResultArray;

}

function get_client_ip()
{
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if (isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if (isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if (isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if (isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

function DecryptDataWithPrivateKey($crypttext)
{
    $merchantPrivateKey = "MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQC3sECMILUvlRiEv8sbM9+QXRG6QSqiNxhkfnArht2eI0c97hYEQgvwzssPR38zmWGlTZ1m9V8dIYIhIF4J+vzA6wnNEIePexWmDUsY9o4ED7J/QGd60gJj8WLqFFXbz4W3YcPhfR2vyAEeRgLvSvuJUSvQn/CWZVeSofzkuOSAlT2233gSWS3laL/E9EBBBMXCe3t2Uh35WI7swUSrYe10/ObhOjdYfIDVFF2hH+5A5i/j1B1gJcpH91NaknbfWDHDCLzYhLqu7RhqTkSVfGqkhSOieLTjMte4oCGItk9GAgXetS0Q9quSoJMDJIvM4J25Iih8nzMgtQM+xxtFrI3TAgMBAAECggEAIHIJdLbySg1T317hSVTIUH+f+f44wZ5puJZ8ybNwwdYJZ27+hJIF+esNAw3l8f9NuB0ViHIe8s5QZfmJosmqi4j4mjgpObbMBCCk+yvRufXr2X5OyOr/cRfaGO6um7z0KYgjSVkIPVxeljJXolzkQUUNMDSQEoCkD0p9mQjvk6Z8JkOddHikAuS90p9/hR6p8vCNNFeaA6DK6sAw0xigLeEa/AM9c+Ll0+qtwYvT1UlwY4v4JWnQzEa7djLhwW5oErcOjCy1/XLACqFWXgAIqTWu/CHqZGGiYbW4wwRNoEm7qGJn4eh4bfLv9eSGfl6lxW0HGwaOFRaZq660OpifAQKBgQDjermhrQBwrohmPGST6LdcLsDPK5oKdAV1K/TqSuXgse3Xex26zXVdcugD4M4bAHoZWsiJZfJJDX968IODxO1DUKnB5P7fXcoFwVCe+AqU0DB/u/42T6I+tuMsZeu52u8lTg34Hi9wXTLExmBMiWx3mYcSuuPM2Vsm5Xn9YRszkQKBgQDOt/7VVRzA2fpXP2Bjh5RghNATrjermUEWUDW3ijSArRe1w+q9fcBVTO6RvaF/A59yEYeEBjAu07B3AB8M1npKPgXJKnjk0p/a/SxUxzuNFrYeHD3YImzjPQDW3du7AA17f4jyr4qseJVN9CGQDQa44AfKCOeaviLd4/8ZFlnxIwKBgA4ySId4foCJw+Vte3FnopnUKSBEvlZMq1KGrcA5cGM3d3ptKovP6rUsSKKFUxeIThZq/fzlKdu5Re1sOSjuD0iWLiH1oppaBhOZ3ugs3qwu2ff+yXymN5/mVgMdUTrGGQqg9mo3qApBJrv1DWl5YhWMtUtL8o+EORYSWCyh9MqhAoGADowcPt8Seu/cEi+/lMGBy5kNG5VzDXxnfC6A2T28nCGSPZRf/J3Q3y/eyhrSNqIGonxhUQUudt0m2fkENl8RlA/xUErQ3ymDXmHasMykgDVM86mBIKiriQJOAND0GaDTUhGTjaCU3bZOq4G2qvB/vJrncWxXACjJQ1w8I4PTKIkCgYBKuBbkP1QfiQfvLF5nM5P7Du++56XPwMjO9s2pF/xRhTI11c9blWfxdhDRK+XoG+S8ZB5nnIF9x74xHKUE2v7ZEviQj3ngBIiB9Pi6AdTAcTfnrP/wJejBi/1yy80KBqtKye4brFPFV3QcjUi5sVykRfoHOWlig2QKzK40sux8aw==";
    $private_key = "-----BEGIN RSA PRIVATE KEY-----\n" . $merchantPrivateKey . "\n-----END RSA PRIVATE KEY-----";
    openssl_private_decrypt(base64_decode($crypttext), $plain_text, $private_key);
    return $plain_text;
}
dd("hello");

date_default_timezone_set('Asia/Dhaka');

$postData = json_decode(file_get_contents("php://input"));

//$MerchantID = "687134349795734";
$MerchantID = "687134349795734";
$DateTime = Date('YmdHis');
$amount = $postData->amount;
$acq_fees = "0" ;
$amount_with_fee = $amount + $acq_fees;
$OrderId = 'Inv'.strtotime("now").rand(1000, 10000);
$random = generateRandomString();
//dd($amount);

$PostURL = "https://api.mynagad.com/api/dfs/check-out/initialize/" . $MerchantID . "/" . $OrderId;
$merchantCallbackURL = "http://202.191.121.20/devtest/merchant-website.php";

$SensitiveData = array(
    'merchantId' => $MerchantID,
    'datetime' => $DateTime,
    'orderId' => $OrderId,
    'challenge' => $random
);


$PostData = array(
    'accountNumber' => '01713730682', //optional
    'dateTime' => $DateTime,
    'sensitiveData' => EncryptDataWithPublicKey(json_encode($SensitiveData)),
    'signature' => SignatureGenerate(json_encode($SensitiveData))
);

// $_SESSION['SensitiveData'] = $SensitiveData;
// $_SESSION['PostData'] = $PostData;

$Result_Data = HttpPostMethod($PostURL, $PostData);


if (isset($Result_Data['sensitiveData']) && isset($Result_Data['signature'])) {
    if ($Result_Data['sensitiveData'] != "" && $Result_Data['signature'] != "") {

        //--------------- Submit Order Start ---------------
        $PlainResponse = json_decode(DecryptDataWithPrivateKey($Result_Data['sensitiveData']), true);

        // print_r ($PlainResponse);
        // exit();

        if (isset($PlainResponse['paymentReferenceId']) && isset($PlainResponse['challenge'])) {

            $paymentReferenceId = $PlainResponse['paymentReferenceId'];
            $randomserver = $PlainResponse['challenge'];

            //$_SESSION['paymentReferenceId'] = $paymentReferenceId;

            //---sensitive data to be encrypted
            $SensitiveDataOrder = array(
                'merchantId' => $MerchantID,
                'orderId' => $OrderId,
                'currencyCode' => '050',
                'amount' => $amount_with_fee,
                'challenge' => $randomserver
            );
            $PostDataOrder = array(
                'sensitiveData' => EncryptDataWithPublicKey(json_encode($SensitiveDataOrder)),
                'signature' => SignatureGenerate(json_encode($SensitiveDataOrder)),
                'merchantCallbackURL' => $merchantCallbackURL,
                'additionalMerchantInfo' => ''
            );

            $OrderSubmitUrl = "https://api.mynagad.com/api/dfs/check-out/complete/" . $paymentReferenceId;
            $Result_Data_Order = HttpPostMethod($OrderSubmitUrl, $PostDataOrder);
                if ($Result_Data_Order['status'] == "Success") {
                    echo json_encode($Result_Data_Order);
                }
                else {
                    echo json_encode($Result_Data_Order);
                }
        } else {
            echo json_encode($PlainResponse);
        }
    }
}


?>
