<?php

$Query_String  = explode("&", explode("?", $_SERVER['REQUEST_URI'])[1] );

$payment_ref_id = substr($Query_String[2], 15); 

$url = "https://api.mynagad.com/api/dfs/verify/payment/". $payment_ref_id;

$ch = curl_init();
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_URL,$url);
$result=curl_exec($ch);
curl_close($ch);

$PlainResponse = json_decode($result, true);

$json_output = '{
    "Order ID" : "'.$PlainResponse["orderId"].'",
    "Amount" : "'.$PlainResponse["amount"].'",
    "Payment Date Time" : "'.$PlainResponse["issuerPaymentDateTime"].'", 
    "Transaction ID" : "'.$PlainResponse["issuerPaymentRefNo"].'", 
    "Status" : "'.$PlainResponse["status"].'", 
    "Status Code" : "'.$PlainResponse["statusCode"].'"
 }';
 

if ($PlainResponse['status'] != 'Success'){
     echo "Payment Failed";
}
else{
     echo "Payment Succeed";
}
          
?>