<?php

Route::group(['prefix' => '/v1', 'namespace' => 'Api\V1', 'as' => 'api.'], function () {
    //Manage User
    Route::post('register', 'AuthController@appUserRegistration');
    Route::post('login', 'AuthController@appUserLogin');
    Route::post('user-details', 'AuthController@appUserDetails');
    Route::post('profile-update', 'AuthController@appUserProfileUpdate');
    Route::post('send-sms', 'AuthController@sendSms');
    Route::post('forgot-password-send-sms', 'AuthController@changePasswordSms');
    Route::post('forgot-password', 'AuthController@forgotPassword');

    //curl test
    Route::get('curl-test', 'AuthController@curlTest');

    //Manage Dashboard
    Route::post('top-scorer', 'GamesController@topScorer');


    //Manage Games
    Route::post('categories', 'QuestionController@getAllCategories');
    Route::post('questions', 'QuestionController@getAllQuestions');
    Route::post('random_questions', 'QuestionController@getRandomQuestions');
    Route::post('daily_questions', 'QuestionController@getDailyQuestions');
    Route::post('read_questions', 'QuestionController@getReadQuestionsDependOnCategory');
    Route::get('result-generate', 'QuestionController@generateResult');
    Route::post('store-coin', 'GamesController@storeCoins');
    Route::post('question-levels', 'GamesController@levels');


    //Manage Puzzle Gamesb
    Route::post('get-puzzle', 'PuzzleController@getAllPuzzles');
    Route::post('get-user-puzzle-story', 'PuzzleController@getUserPuzzleStories');
    Route::post('get-user-puzzle-summery', 'PuzzleController@getUserPuzzleSummery');

    //Manage Package category and package
    Route::post('package-category', 'PackageController@getAllCategory');
    Route::post('package-category/packages', 'PackageController@getPackageDependOnCategory');

    //Manage Transaction
    Route::post('transaction-store', 'TransactionController@store');

    //Balance Transfer
    Route::post('balance-transfer', 'TransactionController@balanceTransfer');
    Route::post('refer-point-transfer', 'TransactionController@referPointTransfer');

    //Reduce Life
    Route::post('life-decrease', 'GamesController@lifeDecrease');

    //Member
    Route::post('add-member', 'MemberController@createMember');

    //Coupon
    Route::post('get-coupons', 'CouponController@getCoupons');
    Route::post('buy-coupons', 'CouponController@buyCoupons');
    Route::post('get-user-coupons', 'CouponController@getUserCoupons');
    Route::post('transfer-coupon', 'CouponController@CouponTransfer');
    Route::post('get-agent-coupons', 'CouponController@getAgentCoupons');

    //Spin
    Route::post('get-spin-questions', 'SpinController@getAllQuestions');

    //Live
    Route::post('get-live-data', 'LiveController@index');
    Route::post('interested-to-join-live', 'LiveController@interestedToJoinLive');
    Route::post('get-last-live-results', 'LiveController@lastResults');
    Route::post('store-live-data', 'LiveController@store');

});
