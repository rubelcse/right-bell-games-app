@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.packages.packages')</h3>

    <p>
        <a href="{{ route('packages.create') }}" class="btn btn-success">@lang('quickadmin.add_new')</a>
    </p>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.list')
        </div>
        <div class="panel-body">
            <table class="table table-bordered table-striped {{ count($packages) > 0 ? 'datatable' : '' }}">
                <thead>
                    <tr>
                        <th style="text-align:center;">Sr</th>
                        <th>Name</th>
                        <th>Category</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th>Action</th>
                    </tr>
                </thead>
                
                <tbody>
                    @if (count($packages) > 0)
                        @foreach ($packages as $key => $package)
                            <tr data-entry-id="{{ $package->id }}">
                                <td>{{++$key}}</td>
                                <td>{{ $package->name }}</td>
                                <td>{{ $package->category->name }}</td>
                                <td>{{ $package->quantity }}</td>
                                <td>{{ $package->price }}</td>
                                <td>
                                    <a href="{{ route('packages.show',[$package->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.view')</a>
                                    <a href="{{ route('packages.edit',[$package->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.edit')</a>
                                   {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.are_you_sure")."');",
                                        'route' => ['packages.destroy', $package->id])) !!}
                                    {!! Form::submit(trans('quickadmin.delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="3">@lang('quickadmin.no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript')
   {{-- <script>
        window.route_mass_crud_entries_destroy = '{{ route('stages.mass_destroy') }}';
    </script>--}}
@endsection