@extends('layouts.app')

@section('content')
    <h3 class="page-title">Register User List</h3>

    <p style="color: green;">
        <strong>From : {{$live->from}}</strong>
    </p>
    <p style="color: green;">
        <strong>To : {{$live->to}}</strong>
    </p>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.list')
        </div>

        <div class="panel-body">
            <table class="table table-bordered table-striped {{ count($live->interestedParticipates) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                    <th>SL</th>
                    <th>User</th>
                    <th>Mobile No</th>
                    <th>District</th>
                    <th>Image</th>
                </tr>
                </thead>

                <tbody>
                @if (count($live->interestedParticipates) > 0)
                    @foreach ($live->interestedParticipates as $key => $user)

                        <tr>
                            <td>{{ ++$key }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->phone_no }}</td>
                            <td>{{ $user->district }}</td>
                            <td>
                                @if(!empty($user->profile_image))
                                    <img style="border-radius: 50% !important;" src="{{asset($user->profile_image)}}" width="65px" height="65px" alt="Profile Image">
                                @else
                                    <img src="{{asset("default/user.png")}}" width="65px" height="65px" alt="Profile Image">
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="3">@lang('quickadmin.no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript')
    {{-- <script>
         window.route_mass_crud_entries_destroy = '{{ route('spin_topic.mass_destroy') }}';
     </script>--}}
@endsection