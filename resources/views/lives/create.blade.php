@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.live.Live')</h3>
    {!! Form::open(['method' => 'POST', 'route' => ['lives.store']]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.create')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-xs-6 form-group">
                    {!! Form::label('from', 'From*', ['class' => 'control-label']) !!}
                    <div class='input-group date date-timepicker'>
                        <input type='text' name="from" class="form-control"/>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span>
                        </span>
                    </div>
                </div>
                <div class="col-xs-6 form-group">
                    {!! Form::label('to', 'To*', ['class' => 'control-label']) !!}
                    <div class='input-group date date-timepicker'>
                        <input name="to" type='text' class="form-control"/>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('point_qty', 'Point To Be Need*', ['class' => 'control-label']) !!}
                    {!! Form::number('point_qty', null, ['class'=>'form-control', 'min'=>0]) !!}
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('from', 'Prize*', ['class' => 'control-label']) !!}
                    {!! Form::textarea('prize', null, ['class'=>'form-control']) !!}
                </div>
            </div>

        </div>
    </div>

    {!! Form::submit(trans('quickadmin.save'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop
@section('javascript')
    <script type="text/javascript">
        $(function () {
            $('.date-timepicker').datetimepicker({
                format: 'YYYY-MM-DD HH:mm'
            });
        });
    </script>
@endsection

