@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.live.Live')</h3>

    <p>
        <a href="{{ route('lives.create') }}" class="btn btn-success">@lang('quickadmin.add_new')</a>
    </p>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.list')
        </div>

        <div class="panel-body">
            <table class="table table-bordered table-striped {{ count($lives) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                    <th>SL</th>
                    <th>From</th>
                    <th>To</th>
                    <th>Prize</th>
                    <th>Point To Be Need</th>
                    <th>Action</th>
                </tr>
                </thead>

                <tbody>
                @if (count($lives) > 0)
                    @foreach ($lives as $key => $live)
                        <tr>
                            <td>{{ ++$key }}</td>
                            <td>{{ $live->from }}</td>
                            <td>{{ $live->to }}</td>
                            <td>{{ $live->prize }}</td>
                            <td>{{ $live->point_qty }}</td>
                            <td>
                                {{--
                                                                    <a href="{{ route('lives.show',[$live->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.view')</a>
                                --}}
                                <a href="{{ route('lives.edit',[$live->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.edit')</a>
                                <a href="{{ route('lives.results',[$live->id]) }}" class="btn btn-xs btn-primary">Results</a>
                                <a href="{{ route('lives.register',[$live->id]) }}" class="btn btn-xs btn-success">
                                    <i class="fa fa-users" aria-hidden="true"></i>
                                    Register User
                                </a>
                                {{-- {!! Form::open(array(
                                     'style' => 'display: inline-block;',
                                     'method' => 'DELETE',
                                     'onsubmit' => "return confirm('".trans("quickadmin.are_you_sure")."');",
                                     'route' => ['lives.destroy', $live->id])) !!}
                                 {!! Form::submit(trans('quickadmin.delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                 {!! Form::close() !!}--}}
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="3">@lang('quickadmin.no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript')
    {{-- <script>
         window.route_mass_crud_entries_destroy = '{{ route('spin_topic.mass_destroy') }}';
     </script>--}}
@endsection