@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.users.user_levels')</h3>

 {{--   <p>
        <a href="{{ route('coupons.create') }}" class="btn btn-success">@lang('quickadmin.add_new')</a>
    </p>--}}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.list')
        </div>

        <div class="panel-body">
            <table class="table table-bordered table-striped {{ count($items) > 0 ? 'datatable' : '' }}">
                <thead>
                    <tr>
                        <th style="text-align:center;">Sr</th>
                        <th>User Name</th>
                        <th class="text-center success">Level</th>
                        <th class="text-center warning">Refer Point</th>
                        <th>Phone No</th>
                        <th>Address</th>
                        <th>District</th>
                    </tr>
                </thead>
                
                <tbody>
                    @if (count($items) > 0)
                        @foreach ($items as $key => $item)
                            <tr data-entry-id="{{ $item->id }}">
                                <td>{{++$key}}</td>
                                <td>{{ $item->user->name }}</td>
                                <td class="text-center success">{{ $item->level }}</td>
                                <td class="text-right warning">{{ $item->user->refer_points }}</td>
                                <td>{{ $item->user->phone_no }}</td>
                                <td>{{ $item->user->address }}</td>
                                <td>{{ $item->user->district }}</td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="3">@lang('quickadmin.no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript')
   {{-- <script>
        window.route_mass_crud_entries_destroy = '{{ route('stages.mass_destroy') }}';
    </script>--}}
@endsection