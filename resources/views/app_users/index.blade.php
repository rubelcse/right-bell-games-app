@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.app_users.users')</h3>

    {{--  <p>
          <a href="{{ route('users.create') }}" class="btn btn-success">@lang('quickadmin.add_new')</a>
      </p>--}}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.list')
        </div>

        <div class="panel-body">
            <table class="table table-bordered table-striped {{ count($appUsers) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                    <th style="text-align:center;">Sr</th>
                    <th>Name</th>
                    <th>Phone No</th>
                    <th>District</th>
                    <th>Refer Code</th>
                    <th>Refer Point</th>
                    <th>Point</th>
                    <th>Image</th>
                    <th>Is Member</th>
                    <th>Is Agent</th>
                    <th>Action</th>
                </tr>
                </thead>

                <tbody>
                @if (count($appUsers) > 0)
                    @foreach ($appUsers as $key => $user)
                        <tr data-entry-id="{{ $user->id }}">
                            <td>{{++$key}}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->phone_no }}</td>
                            <td>{{ $user->district }}</td>
                            <td class="text-center"><span class="label label-success">{{ $user->refer_code }}</span>
                            </td>
                            <td>{{ $user->refer_points }}</td>
                            <td>{{ $user->points }}</td>
                            <td>
                                @if(!empty($user->profile_image))
                                    <img style="border-radius: 50% !important;" src="{{asset($user->profile_image)}}"
                                         width="65px" height="65px" alt="Profile Image">
                                @else
                                    <img src="{{asset("default/user.png")}}" alt="Profile Image">
                                @endif
                            </td>
                            <td class="text-center">
                                @if($user->is_membership == 1)
                                    <span class="label label-primary">Yes</span>
                                @else
                                    <span class="label label-danger">No</span>
                                @endif
                            </td>
                            <td class="text-center">
                                @if($user->is_agent == 1)
                                    <span class="label label-primary">Yes</span>
                                @else
                                    <span class="label label-danger">No</span>
                                @endif
                            </td>
                            <td class="text-center">
                                @if($user->is_agent == 1)
                                    <a href="{{ route('app-users.edit',[$user->id]) }}" class="btn btn-xs btn-danger">Remove
                                        Agent</a>
                                @else
                                    <a href="{{ route('app-users.edit',[$user->id]) }}" class="btn btn-xs btn-primary">Mark
                                        As Agent</a>
                                @endif
                            </td>
                            {{--  <td>
                                  <a href="{{ route('users.show',[$user->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.view')</a>
                                  <a href="{{ route('users.edit',[$user->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.edit')</a>
                                  {!! Form::open(array(
                                      'style' => 'display: inline-block;',
                                      'method' => 'DELETE',
                                      'onsubmit' => "return confirm('".trans("quickadmin.are_you_sure")."');",
                                      'route' => ['users.destroy', $user->id])) !!}
                                  {!! Form::submit(trans('quickadmin.delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                  {!! Form::close() !!}
                              </td>--}}
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="7">@lang('quickadmin.no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript')
    {{-- <script>
         window.route_mass_crud_entries_destroy = '{{ route('users.mass_destroy') }}';
     </script>--}}
@endsection