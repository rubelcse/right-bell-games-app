@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.questionLevel.title')</h3>

    <p>
        <a href="{{ route('questionLevel.create') }}" class="btn btn-success">@lang('quickadmin.add_new')</a>
    </p>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.list')
        </div>
        <div class="panel-body">
            <table class="table table-bordered table-striped {{ count($levels) > 0 ? 'datatable' : '' }}">
                <thead>
                    <tr>
                        <th style="text-align:center;">Sr</th>
                        <th>Title</th>
                        <th>No Of Playable Question</th>
                        <th>Total Playable Time</th>
                        <th class="text-center">Level Cost</th>
                        <th>Action</th>
                    </tr>
                </thead>
                
                <tbody>
                    @if (count($levels) > 0)
                        @foreach ($levels as $key => $level)
                            <tr>
                                <td>{{++$key}}</td>
                                <td>{{ $level->title or null}}</td>
                                <td>{{ $level->playable_question or null}}</td>
                                <td>{{ $level->playable_time or null }}</td>
                                <td class="success text-center">{{ $level->level_cost or null }}</td>
                                <td>
                                   {{-- <a href="{{ route('questionLevel.show',[$level->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.view')</a>--}}
                                    <a href="{{ route('questionLevel.edit',[$level->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.edit')</a>
                                  {{-- {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.are_you_sure")."');",
                                        'route' => ['questionLevel.destroy', $level->id])) !!}
                                    {!! Form::submit(trans('quickadmin.delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}--}}
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="3">@lang('quickadmin.no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript')
   {{-- <script>
        window.route_mass_crud_entries_destroy = '{{ route('stages.mass_destroy') }}';
    </script>--}}
@endsection