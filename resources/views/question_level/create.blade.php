@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.questionLevel.title')</h3>
    {!! Form::open(['method' => 'POST', 'route' => ['questionLevel.store']]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.create')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-xs-6 form-group">
                    {!! Form::label('title', 'Title*', ['class' => 'control-label']) !!}
                    {!! Form::text('title', old('title'), ['class' => 'form-control', 'placeholder' => 'Enter Title']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('title'))
                        <p class="help-block">
                            {{ $errors->first('title') }}
                        </p>
                    @endif
                </div>
                <div class="col-xs-6 form-group">
                    {!! Form::label('level_cost', 'Level Cost*', ['class' => 'control-label']) !!}
                    {!! Form::text('level_cost', old('level_cost'), ['class' => 'form-control', 'placeholder' => 'Enter Title']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('level_cost'))
                        <p class="help-block">
                            {{ $errors->first('level_cost') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6 form-group">
                    {!! Form::label('playable_question', 'No Of Playable Question*', ['class' => 'control-label']) !!}
                    {!! Form::number('playable_question', old('playable_question'), ['class' => 'form-control', 'placeholder' => 'Enter Playable Question Quantity']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('playable_question'))
                        <p class="help-block">
                            {{ $errors->first('playable_question') }}
                        </p>
                    @endif
                </div>
                <div class="col-xs-6 form-group">
                    {!! Form::label('playable_time', 'Total Playable Time*', ['class' => 'control-label']) !!}
                    {!! Form::number('playable_time', old('playable_time'), ['class' => 'form-control', 'placeholder' => 'Enter Playable Time']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('playable_time'))
                        <p class="help-block">
                            {{ $errors->first('playable_time') }}
                        </p>
                    @endif
                </div>
            </div>
        </div>
    </div>

    {!! Form::submit(trans('quickadmin.save'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop
{{--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="{{asset('quickadmin/js/moment.min.js')}}"></script>
<script src="{{asset('quickadmin/js/bootstrap-datetimepicker.js')}}"></script>
<script src="{{asset('quickadmin/js/bootstrap-timepicker.js')}}"></script>
<script type="text/javascript">
    $(function () {
        $('.time').datetimepicker({
            format: 'LT'
        });
    });
</script>--}}

