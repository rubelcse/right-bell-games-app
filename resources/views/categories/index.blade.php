@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.packages.category')</h3>

  {{--  <p>
        <a href="{{ route('packageCategory.create') }}" class="btn btn-success">@lang('quickadmin.add_new')</a>
    </p>--}}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.list')
        </div>

        <div class="panel-body">
            <table class="table table-bordered table-striped {{ count($categories) > 0 ? 'datatable' : '' }}">
                <thead>
                    <tr>
                        <th>Sr</th>
                        <th>Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
                
                <tbody>
                    @if (count($categories) > 0)
                        @foreach ($categories as $key => $category)
                            <tr data-entry-id="{{ $category->id }}">
                                <td>{{++$key}}</td>
                                <td>{{ $category->name }}</td>
                                <td>
                                    <a href="{{ route('packageCategory.show',[$category->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.view')</a>
                                    <a href="{{ route('packageCategory.edit',[$category->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.edit')</a>
                                   {{-- {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.are_you_sure")."');",
                                        'route' => ['packageCategory.destroy', $category->id])) !!}
                                    {!! Form::submit(trans('quickadmin.delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}--}}
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="3">@lang('quickadmin.no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript')
   {{-- <script>
        window.route_mass_crud_entries_destroy = '{{ route('packageCategory.mass_destroy') }}';
    </script>--}}
@endsection