@extends('layouts.app')

<style>
    #pattern-box {
        display: none;
    }
    #submit {
        display: none;
    }
    #loader {
        display: none;
    }
</style>

@section('content')
    <h3 class="page-title">@lang('quickadmin.puzzles.puzzles')</h3>
    {!! Form::open(['method' => 'POST', 'route' => ['puzzles.store']]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.create')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-xs-6 form-group">
                    {!! Form::label('stage_id', 'Stage*', ['class' => 'control-label', 'id' => 'stage-id']) !!}
                    {!! Form::select('stage_id', $stages, null , ['class' => 'form-control']) !!}
                </div>
                <div class="col-xs-6 form-group">
                    {!! Form::label('level', 'Level*', ['class' => 'control-label', 'id' => 'level-id']) !!}
                    {!! Form::select('level', ['' => '- Select levels -'], null, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="row row-centered">
                <div class="col-xs-6 col-centered">
                    <div id="pattern-box"></div>
                </div>
            </div>
            <div class="row row-centered">
                <div class="col-xs-6 col-centered">
                    {!! Form::submit(trans('quickadmin.save'), ['class' => 'btn btn-danger btn-block', 'id' => 'submit']) !!}
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
   {{-- <div id='loader' style='display: none;'>
        <img src='{{asset('/quickadmin/images/loading.gif')}}' width='32px' height='32px'>
    </div>--}}
    <div id="loader" style="margin: 0px; padding: 0px; position: fixed; right: 0px; top: 0px; width: 100%; height: 100%; background-color: rgba(102, 102, 102, 0.66);; z-index: 30001; opacity: 0.8;">
        <p style="position: absolute; color: White; top: 50%; left: 45%;">
            <img src='{{asset('/quickadmin/images/loading.gif')}}'>
        </p>
    </div>
@stop
@section('javascript')
    <script>
        $(function () {
            //stage wise level load
            $("select[name='stage_id']").change(function () {
                $('#pattern-box').hide();
                $('#submit').hide();
                $("#loader").hide();
                $("select[name='level']").empty();
                var stage_id = $(this).val();
                var token = $("input[name='_token']").val();
                if(stage_id !== '') {
                    $.ajax({
                        url: "<?php echo route('stage-wise-level') ?>",
                        method: 'POST',
                        data: {stage_id: stage_id, _token: token},
                        beforeSend: function(){
                            // Show image container
                            $("#loader").show();
                        },
                        success: function (data) {
                            //alert(data.options);
                            // $("select[name='level']").html('');
                            $("select[name='level']").html(data.options);
                            $("#loader").hide();
                        }
                    });
                }
            });
            //stage level wise pattern schema load
            $("select[name='level']").change(function () {
                $("#pattern-box").empty();
                $('#pattern-box').hide();
                $("#loader").hide();
                $('#submit').hide();
                var level = $(this).val();
                var stage_id = $("select[name='stage_id']").val();
                var token = $("input[name='_token']").val();
                //alert(level);
                if (level !== '' && stage_id !== '') {
                    $.ajax({
                        url: "<?php echo route('pattern-schema') ?>",
                        method: 'POST',
                        data: {stage_id: stage_id, level: level, _token: token},
                        beforeSend: function(){
                            // Show image container
                            $("#loader").show();
                        },
                        success: function (data) {
                            $('#pattern-box').show();
                            $('#submit').show();
                            //$("#pattern-box").html('');
                            $("#pattern-box").html(data.schema);
                            $("#loader").hide();
                        }
                    });
                }
            });
        });
    </script>
@endsection

