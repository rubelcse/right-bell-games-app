@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.live.question_stage')</h3>
    
    {!! Form::model($questionStage, ['method' => 'PUT', 'route' => ['question_stages.update', $questionStage->id]]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.edit')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-xs-6 form-group">
                    {!! Form::label('name', 'Name*', ['class' => 'control-label']) !!}
                    {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => 'Enter Name']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('name'))
                        <p class="help-block">
                            {{ $errors->first('name') }}
                        </p>
                    @endif
                </div>
                <div class="col-xs-6 form-group">
                    {!! Form::label('question_options', 'Question Option*', ['class' => 'control-label']) !!}
                    {!! Form::text('question_options', old('question_options'), ['class' => 'form-control', 'disabled' => true]) !!}
                    <p class="help-block"></p>
                    @if($errors->has('question_options'))
                        <p class="help-block">
                            {{ $errors->first('question_options') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6 form-group">
                    {!! Form::label('mark', 'Mark*', ['class' => 'control-label']) !!}
                    {!! Form::number('mark', old('mark'), ['class' => 'form-control', 'placeholder' => 'Enter Mark','step' => 'any']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('mark'))
                        <p class="help-block">
                            {{ $errors->first('mark') }}
                        </p>
                    @endif
                </div>
                <div class="col-xs-6 form-group">
                    {!! Form::label('negative_mark', 'Negative Mark*', ['class' => 'control-label']) !!}
                    {!! Form::number('negative_mark', old('negative_mark'), ['class' => 'form-control', 'placeholder' => 'Enter Mark', 'step' => 'any']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('negative_mark'))
                        <p class="help-block">
                            {{ $errors->first('negative_mark') }}
                        </p>
                    @endif
                </div>
                <div class="col-xs-6 form-group">
                    {!! Form::label('take_question_limit', 'Question Limit*', ['class' => 'control-label']) !!}
                    {!! Form::number('take_question_limit', old('take_question_limit'), ['class' => 'form-control', 'placeholder' => 'Enter Question Limit']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('take_question_limit'))
                        <p class="help-block">
                            {{ $errors->first('take_question_limit') }}
                        </p>
                    @endif
                </div>
                <div class="col-xs-6 form-group">
                    {!! Form::label('status', 'Status*', ['class' => 'control-label']) !!}
                    {!! Form::select('status', $status, old('status'), ['class' => 'form-control']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('status'))
                        <p class="help-block">
                            {{ $errors->first('status') }}
                        </p>
                    @endif
                </div>
            </div>
        </div>
    </div>

    {!! Form::submit(trans('quickadmin.update'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

