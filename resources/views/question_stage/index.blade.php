@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.live.question_stage')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.list')
        </div>
        <div class="panel-body">
            <table class="table table-bordered table-striped {{ count($items) > 0 ? 'datatable' : '' }}">
                <thead>
                    <tr>
                        <th style="text-align:center;">Sr</th>
                        <th>Name</th>
                        <th>No Of Options</th>
                        <th>Mark</th>
                        <th class="text-center">Negative Mark</th>
                        <th class="text-center">Question Limit</th>
                        <th class="text-center">Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                
                <tbody>
                    @if (count($items) > 0)
                        @foreach ($items as $key => $item)
                            <tr>
                                <td>{{++$key}}</td>
                                <td>{{ $item->name or null}}</td>
                                <td class="text-center">{{ $item->question_options or null}}</td>
                                <td class="text-center">{{ $item->mark or null }}</td>
                                <td class="text-center">{{ $item->negative_mark or null }}</td>
                                <td class="text-center">{{ $item->take_question_limit or null }}</td>
                                <td class="text-center">
                                    @if($item->status == 1)
                                        <span class="badge badge-success">Active</span>
                                        @else
                                        <span class="badge badge-warning">InActive</span>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ route('question_stages.edit',[$item->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.edit')</a>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="3">@lang('quickadmin.no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript')
   {{-- <script>
        window.route_mass_crud_entries_destroy = '{{ route('stages.mass_destroy') }}';
    </script>--}}
@endsection