@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.question.spinQuestions')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.view')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>Question</th>
                            <td>{{ $question->question or '' }}</td>
                        </tr>
                        @foreach($question->options as $key => $option)
                            <tr>
                                <th>Option-{{++$key}}</th>
                                <td>{{ $option->option or '' }}</td>
                            </tr>
                        @endforeach
                        <tr>
                            <th>Answer</th>
                            <td>{!! $question->correctOptions->option !!}</td>
                        </tr>
                    </table>
                </div>
            </div>

            <p>&nbsp;</p>

            <a href="{{ route('spinQuestions.index') }}" class="btn btn-default">@lang('quickadmin.back_to_list')</a>
        </div>
    </div>
@stop