@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.question.spinQuestions')</h3>
    
    {!! Form::model($spinQuestion, ['method' => 'PUT', 'route' => ['spinQuestions.update', $spinQuestion->id]]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.edit')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('spin_topic_id ', 'Topic*', ['class' => 'control-label']) !!}
                    {!! Form::select('spin_topic_id', $topics, old('spin_topic_id'), ['class' => 'form-control']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('spin_topic_id'))
                        <p class="help-block">
                            {{ $errors->first('spin_topic_id') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('question', 'Question text*', ['class' => 'control-label']) !!}
                    {!! Form::textarea('question', old('question'), ['class' => 'form-control ', 'placeholder' => '', 'rows'=>'2']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('question'))
                        <p class="help-block">
                            {{ $errors->first('question') }}
                        </p>
                    @endif
                </div>
            </div>
        </div>
    </div>

    {!! Form::submit(trans('quickadmin.update'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

