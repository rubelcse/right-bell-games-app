@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.question.spinTopics')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.view')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.topics.fields.title')</th>
                            <td>{{ $spinTopic->title }}</td>
                        </tr>
                        <tr>
                            <th>Question Limit</th>
                            <td>{{ $spinTopic->take_question_limit }}</td>
                        </tr>
                    </table>
                </div>
            </div>

            <p>&nbsp;</p>

            <a href="{{ route('spinTopics.index') }}" class="btn btn-default">@lang('quickadmin.back_to_list')</a>
        </div>
    </div>
@stop