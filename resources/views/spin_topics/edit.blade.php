@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.question.spinTopics')</h3>
    
    {!! Form::model($spinTopic, ['method' => 'PUT', 'route' => ['spinTopics.update', $spinTopic->id]]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.edit')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-xs-6 form-group">
                    {!! Form::label('title', 'Title*', ['class' => 'control-label']) !!}
                    {!! Form::text('title', old('title'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('title'))
                        <p class="help-block">
                            {{ $errors->first('title') }}
                        </p>
                    @endif
                </div>
                <div class="col-xs-6 form-group">
                    {!! Form::label('take_question_limit', 'Take Question Limit In This Category*', ['class' => 'control-label']) !!}
                    {!! Form::number('take_question_limit', old('take_question_limit'), ['class' => 'form-control', 'placeholder' => 'Enter Limit']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('take_question_limit'))
                        <p class="help-block">
                            {{ $errors->first('take_question_limit') }}
                        </p>
                    @endif
                </div>
            </div>
            
        </div>
    </div>

    {!! Form::submit(trans('quickadmin.update'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

