@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.levels.levels')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.view')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>Name</th>
                            <td>{{ $level->name }}</td>
                        </tr>
                        <tr>
                            <th>Stage</th>
                            <td>{{ $level->stage->name }}</td>
                        </tr>
                        <tr>
                            <th>Level</th>
                            <td>{{ $level->level }}</td>
                        </tr>
                        <tr>
                            <th>No of Moves</th>
                            <td>{{ $level->no_of_moves }}</td>
                        </tr>
                        <tr>
                            <th>Point</th>
                            <td>{{ $level->point }}</td>
                        </tr>
                    </table>
                </div>
            </div>

            <p>&nbsp;</p>

            <a href="{{ route('levels.index') }}" class="btn btn-default">@lang('quickadmin.back_to_list')</a>
        </div>
    </div>
@stop