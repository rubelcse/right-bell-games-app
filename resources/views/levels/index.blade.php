@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.levels.levels')</h3>

    <p>
        <a href="{{ route('levels.create') }}" class="btn btn-success">@lang('quickadmin.add_new')</a>
    </p>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.list')
        </div>

        <div class="panel-body">
            <table class="table table-bordered table-striped {{ count($levels) > 0 ? 'datatable' : '' }}">
                <thead>
                    <tr>
                        <th style="text-align:center;">Sr</th>
                        <th>Name</th>
                        <th>Stage</th>
                        <th>Level</th>
                        <th>No of Moves</th>
                        <th>Point</th>
                        <th class="text-center">Level Cost 2</th>
                        <th class="text-center">Level Cost 3</th>
                        <th class="text-center">Level Cost 4</th>
                        <th>Action</th>
                    </tr>
                </thead>
                
                <tbody>
                    @if (count($levels) > 0)
                        @foreach ($levels as $key => $level)
                            <tr data-entry-id="{{ $level->id }}">
                                <td>{{++$key}}</td>
                                <td>{{ $level->name }}</td>
                                <td>{{ $level->stage->name }}</td>
                                <td>{{ $level->level }}</td>
                                <td>{{ $level->no_of_moves }}</td>
                                <td>{{ $level->point }}</td>
                                <td class="text-center success">{{ $level->level_cost_2 or null}}</td>
                                <td class="text-center info">{{ $level->level_cost_3 or null}}</td>
                                <td class="text-center warning">{{ $level->level_cost_4 or null}}</td>
                                <td>
                                    <a href="{{ route('levels.show',[$level->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.view')</a>
                                    <a href="{{ route('levels.edit',[$level->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.edit')</a>
                                   {{-- {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.are_you_sure")."');",
                                        'route' => ['stages.destroy', $level->id])) !!}
                                    {!! Form::submit(trans('quickadmin.delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}--}}
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="3">@lang('quickadmin.no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript')
   {{-- <script>
        window.route_mass_crud_entries_destroy = '{{ route('stages.mass_destroy') }}';
    </script>--}}
@endsection