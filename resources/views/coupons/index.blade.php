@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.coupon.coupon')</h3>

    <p>
        <a href="{{ route('coupons.create') }}" class="btn btn-success">@lang('quickadmin.add_new')</a>
    </p>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.list')
        </div>

        <div class="panel-body">
            <table class="table table-bordered table-striped {{ count($items) > 0 ? 'datatable' : '' }}">
                <thead>
                    <tr>
                        <th style="text-align:center;">Sr</th>
                        <th>Name</th>
                        <th>Need Point</th>
                        <th>Amount</th>
                        <th>Action</th>
                    </tr>
                </thead>
                
                <tbody>
                    @if (count($items) > 0)
                        @foreach ($items as $key => $item)
                            <tr data-entry-id="{{ $item->id }}">
                                <td>{{++$key}}</td>
                                <td>{{ $item->name }}</td>
                                <td>{{ $item->need_point }}</td>
                                <td>{{ $item->amount }}</td>
                                <td>
                                    <a href="{{ route('coupons.show',[$item->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.view')</a>
                                    <a href="{{ route('coupons.edit',[$item->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.edit')</a>
                                   {{-- {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.are_you_sure")."');",
                                        'route' => ['stages.destroy', $item->id])) !!}
                                    {!! Form::submit(trans('quickadmin.delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}--}}
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="3">@lang('quickadmin.no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript')
   {{-- <script>
        window.route_mass_crud_entries_destroy = '{{ route('stages.mass_destroy') }}';
    </script>--}}
@endsection