@inject('request', 'Illuminate\Http\Request')
<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu"
            data-keep-expanded="false"
            data-auto-scroll="true"
            data-slide-speed="200">

            {{--<li class="{{ $request->segment(1) == 'tests' ? 'active' : '' }}">
                <a href="{{ route('tests.index') }}">
                    <i class="fa fa-gears"></i>
                    <span class="title">@lang('quickadmin.test.new')</span>
                </a>
            </li>--}}

            {{--  <li class="{{ $request->segment(1) == 'results' ? 'active' : '' }}">
                  <a href="{{ route('results.index') }}">
                      <i class="fa fa-gears"></i>
                      <span class="title">@lang('quickadmin.results.title')</span>
                  </a>
              </li>--}}

            @if(Auth::user()->isAdmin())
               {{-- <li class="{{ $request->segment(1) == 'topics' ? 'active' : '' }}">
                    <a href="{{ route('topics.index') }}">
                        <i class="fa fa-gears"></i>
                        <span class="title">@lang('quickadmin.topics.title')</span>
                    </a>
                </li>--}}
                <li>
                    <a href="#">
                        <i class="fa fa-gears"></i>
                        <span class="title">@lang('quickadmin.question.manage_question')</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="{{ $request->segment(1) == 'questionLevel' ? 'active active-sub' : '' }}">
                            <a href="{{ route('questionLevel.index') }}">
                                <i class="fa fa-gears"></i>
                                <span class="title">@lang('quickadmin.questionLevel.title')</span>
                            </a>
                        </li>
                        <li class="{{ $request->segment(1) == 'questionLevelDetail' ? 'active active-sub' : '' }}">
                            <a href="{{ route('questionLevelDetail.index') }}">
                                <i class="fa fa-gears"></i>
                                <span class="title">@lang('quickadmin.questionLevel.details')</span>
                            </a>
                        </li>
                        <li class="{{ $request->segment(1) == 'topics' ? 'active active-sub' : '' }}">
                            <a href="{{ route('topics.index') }}">
                                <i class="fa fa-gears"></i>
                                <span class="title">@lang('quickadmin.topics.title')</span>
                            </a>
                        </li>
                        <li class="{{ $request->segment(1) == 'questions' ? 'active active-sub' : '' }}">
                            <a href="{{ route('questions.index') }}">
                                <i class="fa fa-gears"></i>
                                <span class="title">@lang('quickadmin.questions.title')</span>
                            </a>
                        </li>
                        <li class="{{ $request->segment(1) == 'questions_options' ? 'active active-sub' : '' }}">
                            <a href="{{ route('questions_options.index') }}">
                                <i class="fa fa-gears"></i>
                                <span class="title">@lang('quickadmin.questions-options.title')</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-gears"></i>
                        <span class="title">@lang('quickadmin.question.manage_spin')</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="{{ $request->segment(1) == 'spinTopics' ? 'active active-sub' : '' }}">
                            <a href="{{ route('spinTopics.index') }}">
                                <i class="fa fa-gears"></i>
                                <span class="title">@lang('quickadmin.question.spinTopics')</span>
                            </a>
                        </li>
                        <li class="{{ $request->segment(1) == 'spinQuestions' ? 'active active-sub' : '' }}">
                            <a href="{{ route('spinQuestions.index') }}">
                                <i class="fa fa-gears"></i>
                                <span class="title">@lang('quickadmin.question.spinQuestions')</span>
                            </a>
                        </li>
                        <li class="{{ $request->segment(1) == 'spin_questions_options' ? 'active active-sub' : '' }}">
                            <a href="{{ route('spin_questions_options.index') }}">
                                <i class="fa fa-gears"></i>
                                <span class="title">@lang('quickadmin.question.spinQuestionOption')</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-puzzle-piece"></i>
                        <span class="title">@lang('quickadmin.puzzles.manage_puzzles')</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="{{ $request->segment(1) == 'stages' ? 'active active-sub' : '' }}">
                            <a href="{{ route('stages.index') }}">
                                <i class="fa fa-gears"></i>
                                <span class="title">@lang('quickadmin.puzzles.stages')</span>
                            </a>
                        </li>
                        <li class="{{ $request->segment(1) == 'levels' ? 'active active-sub' : '' }}">
                            <a href="{{ route('levels.index') }}">
                                <i class="fa fa-gears"></i>
                                <span class="title">@lang('quickadmin.puzzles.levels')</span>
                            </a>
                        </li>
                        <li class="{{ $request->segment(1) == 'patterns' ? 'active active-sub' : '' }}">
                            <a href="{{ route('patterns.index') }}">
                                <i class="fa fa-gears"></i>
                                <span class="title">@lang('quickadmin.puzzles.patterns')</span>
                            </a>
                        </li>
                        <li class="{{ $request->segment(1) == 'puzzles' ? 'active active-sub' : '' }}">
                            <a href="{{ route('puzzles.index') }}">
                                <i class="fa fa-gears"></i>
                                <span class="title">@lang('quickadmin.puzzles.puzzles')</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-cart-arrow-down"></i>
                        <span class="title">@lang('quickadmin.packages.manage_packages')</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="{{ $request->segment(1) == 'packageCategory' ? 'active active-sub' : '' }}">
                            <a href="{{ route('packageCategory.index') }}">
                                <i class="fa fa-gears"></i>
                                <span class="title">@lang('quickadmin.packages.category')</span>
                            </a>
                        </li>
                        <li class="{{ $request->segment(1) == 'packages' ? 'active active-sub' : '' }}">
                            <a href="{{ route('packages.index') }}">
                                <i class="fa fa-gears"></i>
                                <span class="title">@lang('quickadmin.packages.packages')</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-gift"></i>
                        <span class="title">@lang('quickadmin.coupon.manage_coupon')</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="{{ $request->segment(1) == 'coupons' ? 'active active-sub' : '' }}">
                            <a href="{{ route('coupons.index') }}">
                                <i class="fa fa-gears"></i>
                                <span class="title">@lang('quickadmin.coupon.coupon')</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-gift"></i>
                        <span class="title">@lang('quickadmin.shop.manage_shop')</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="{{ $request->segment(1) == 'shops' ? 'active active-sub' : '' }}">
                            <a href="{{ route('shops.index') }}">
                                <i class="fa fa-gears"></i>
                                <span class="title">@lang('quickadmin.shop.shop')</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-users"></i>
                        <span class="title">@lang('quickadmin.app_users.manage_users')</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="{{ $request->segment(1) == 'app-users' ? 'active active-sub' : '' }}">
                            <a href="{{ route('app-users.index') }}">
                                <i class="fa fa-user"></i>
                                <span class="title">@lang('quickadmin.app_users.users')</span>
                            </a>
                        </li>
                        <li class="{{ $request->segment(1) == 'user_levels' ? 'active active-sub' : '' }}">
                            <a href="{{ route('user_levels.index') }}">
                                <i class="fa fa-level-down"></i>
                                <span class="title">@lang('quickadmin.users.user_levels')</span>
                            </a>
                        </li>
                       <li class="{{ $request->segment(1) == 'push-notification' ? 'active active-sub' : '' }}">
                            <a href="{{ route('push') }}">
                                <i class="fa fa-paper-plane-o"></i>
                                <span class="title">@lang('quickadmin.push.send_push')</span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="#">
                        <i class="fa fa-line-chart"></i>
                        <span class="title">@lang('quickadmin.live.manage_live')</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="{{ $request->segment(1) == 'question_stages' ? 'active active-sub' : '' }}">
                            <a href="{{ route('question_stages.index') }}">
                                <i class="fa fa-user"></i>
                                <span class="title">@lang('quickadmin.live.question_stage')</span>
                            </a>
                        </li>
                        <li class="{{ $request->segment(1) == 'live_questions' ? 'active active-sub' : '' }}">
                            <a href="{{ route('live_questions.index') }}">
                                <i class="fa fa-level-down"></i>
                                <span class="title">@lang('quickadmin.live.Live Questions')</span>
                            </a>
                        </li>
                       <li class="{{ $request->segment(1) == 'lives' ? 'active active-sub' : '' }}">
                            <a href="{{ route('lives.index') }}">
                                <i class="fa fa-line-chart"></i>
                                <span class="title">@lang('quickadmin.live.Live')</span>
                            </a>
                        </li>
                    </ul>
                </li>


                <li>
                    <a href="#">
                        <i class="fa fa-wrench" aria-hidden="true"></i>
                        <span class="title">@lang('quickadmin.settings.Settings')</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="{{ $request->segment(1) == 'settings' ? 'active active-sub' : '' }}">
                            <a href="{{ route('settings.index') }}">
                                <i class="fa fa-gears"></i>
                                <span class="title">@lang('quickadmin.settings.Settings')</span>
                            </a>
                        </li>
                        <li class="{{ $request->segment(1) == 'pointSettings' ? 'active active-sub' : '' }}">
                            <a href="{{ route('pointSettings.index') }}">
                                <i class="fa fa-gears"></i>
                                <span class="title">@lang('quickadmin.settings.Point Settings')</span>
                            </a>
                        </li>
                        <li class="{{ $request->segment(1) == 'apk-manage' ? 'active active-sub' : '' }}">
                            <a href="{{ route('apk-manage.index') }}">
                                <i class="fa fa-gears"></i>
                                <span class="title">APK Version Manager</span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="#">
                        <i class="fa fa-area-chart" aria-hidden="true"></i>
                        <span class="title">@lang('quickadmin.reports.reports')</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="{{ $request->segment(1) == 'generate-leader-board' ? 'active active-sub' : '' }}">
                            <a href="{{ route('reports.leader_board') }}">
                                <i class="fa fa-mortar-board"></i>
                                <span class="title">@lang('quickadmin.reports.leader_board')</span>
                            </a>
                        </li>
                    </ul>
                </li>
                {{--<li>
                    <a href="#">
                        <i class="fa fa-users"></i>
                        <span class="title">@lang('quickadmin.user-management.title')</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="{{ $request->segment(1) == 'roles' ? 'active active-sub' : '' }}">
                            <a href="{{ route('roles.index') }}">
                                <i class="fa fa-briefcase"></i>
                                <span class="title">
                                    @lang('quickadmin.roles.title')
                                </span>
                            </a>
                        </li>
                        <li class="{{ $request->segment(1) == 'users' ? 'active active-sub' : '' }}">
                            <a href="{{ route('users.index') }}">
                                <i class="fa fa-user"></i>
                                <span class="title">
                                    @lang('quickadmin.users.title')
                                </span>
                            </a>
                        </li>
                        <li class="{{ $request->segment(1) == 'user_actions' ? 'active active-sub' : '' }}">
                            <a href="{{ route('user_actions.index') }}">
                                <i class="fa fa-th-list"></i>
                                <span class="title">
                                    @lang('quickadmin.user-actions.title')
                                </span>
                            </a>
                        </li>
                    </ul>
                </li>--}}
            @endif
            <li>
                <a href="#logout" onclick="$('#logout').submit();">
                    <i class="fa fa-arrow-left"></i>
                    <span class="title">@lang('quickadmin.logout')</span>
                </a>
            </li>
        </ul>
    </div>
</div>
{!! Form::open(['route' => 'auth.logout', 'style' => 'display:none;', 'id' => 'logout']) !!}
<button type="submit">@lang('quickadmin.logout')</button>
{!! Form::close() !!}
