@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.live.Live Questions')</h3>
    {!! Form::open(['method' => 'POST', 'route' => ['live_questions.store']]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.create')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('stage_id', 'Question Stage*', ['class' => 'control-label']) !!}
                    {!! Form::select('stage_id', $stages, old('stage_id'), ['class' => 'form-control']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('stage_id'))
                        <p class="help-block">
                            {{ $errors->first('stage_id') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('question', 'Question text*', ['class' => 'control-label']) !!}
                    {!! Form::textarea('question', old('question'), ['class' => 'form-control ', 'placeholder' => '', 'rows'=>'2']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('question'))
                        <p class="help-block">
                            {{ $errors->first('question') }}
                        </p>
                    @endif
                </div>
            </div>
            <div id="question_options"></div>
        </div>
    </div>
    {!! Form::submit(trans('quickadmin.save'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}

    <div id="loader"
         style="display:none;margin: 0px; padding: 0px; position: fixed; right: 0px; top: 0px; width: 100%; height: 100%; background-color: rgba(102, 102, 102, 0.66);; z-index: 30001; opacity: 0.8;">
        <p style="position: absolute; color: White; top: 50%; left: 45%;">
            <img src='{{asset('/quickadmin/images/loading.gif')}}'>
        </p>
    </div>
@stop

@section('javascript')
    <script>
        $(function () {
            //stage level wise pattern schema load
            $("select[name='stage_id']").change(function () {
                $("#question_options").empty();
                $('#question_options').hide();
                $("#loader").show();
                var stage_id = $(this).val();
                var token = $("input[name='_token']").val();
                //alert(level);
                if (stage_id !== '') {
                    $.ajax({
                        url: "<?php echo route('question-schema') ?>",
                        method: 'POST',
                        data: {stage_id: stage_id, _token: token},
                        beforeSend: function () {
                            // Show image container
                            $("#loader").show();
                        },
                        success: function (data) {
                            $('#question_options').show();
                            $("#question_options").html(data.schema);
                            $("#loader").hide();
                        }
                    });
                }
            });
        });
    </script>
@endsection

