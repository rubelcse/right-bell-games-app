@php
  $answer_options = [
      '' => 'Select Answer'
  ];
@endphp
<div class="row">
    @for ($i = 1; $i <= $no_of_options; $i++)
        @php
            $answer_options = array_merge($answer_options, ["option$i" => "Option #$i"]);
        @endphp
        <div class="col-xs-6 form-group">
            {!! Form::label("option".$i, 'Option #'.$i, ['class' => 'control-label']) !!}
            {!! Form::text('option'.$i, old('option'.$i), ['class' => 'form-control ', 'placeholder' => '']) !!}
        </div>
    @endfor
</div>

<div class="row">
    <div class="col-xs-12 form-group">
        {!! Form::label('answer', 'Answer', ['class' => 'control-label']) !!}
        {!! Form::select('answer', $answer_options, old('answer'), ['class' => 'form-control']) !!}
        <p class="help-block"></p>
        @if($errors->has('answer'))
            <p class="help-block">
                {{ $errors->first('answer') }}
            </p>
        @endif
    </div>
</div>
