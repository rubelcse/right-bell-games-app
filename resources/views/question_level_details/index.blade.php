@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.questionLevel.details')</h3>

    <p>
        <a href="{{ route('questionLevelDetail.create') }}" class="btn btn-success">@lang('quickadmin.add_new')</a>
    </p>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.list')
        </div>
        <div class="panel-body">
            <table class="table table-bordered table-striped {{ count($levelDetails) > 0 ? 'datatable' : '' }}">
                <thead>
                    <tr>
                        <th style="text-align:center;">Sr</th>
                        <th>Level</th>
                        <th>No Of Question Of Section-A</th>
                        <th>Total Point Of Section-A</th>
                        <th>No Of Question Of Section-B</th>
                        <th>Total Point Of Section-B</th>
                        <th>No Of Question Of Section-C</th>
                        <th>Total Point Of Section-C</th>
                        <th>Action</th>
                    </tr>
                </thead>
                
                <tbody>
                    @if (count($levelDetails) > 0)
                        @foreach ($levelDetails as $key => $detail)
                            <tr data-entry-id="{{ $detail->id }}">
                                <td>{{++$key}}</td>
                                <td>{{ $detail->level->title or null }}</td>
                                <td>{{ $detail->section_a_no_of_question or null }}</td>
                                <td>{{ $detail->section_a_no_of_point or null }}</td>
                                <td>{{ $detail->section_b_no_of_question or null }}</td>
                                <td>{{ $detail->section_b_no_of_point or null }}</td>
                                <td>{{ $detail->section_c_no_of_question or null }}</td>
                                <td>{{ $detail->section_c_no_of_point or null }}</td>
                                <td>
                                   {{-- <a href="{{ route('packages.show',[$detail->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.view')</a>--}}
                                    <a href="{{ route('questionLevelDetail.edit',[$detail->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.edit')</a>
                                   {{--{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.are_you_sure")."');",
                                        'route' => ['questionLevelDetail.destroy', $detail->id])) !!}
                                    {!! Form::submit(trans('quickadmin.delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}--}}
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="3">@lang('quickadmin.no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript')
   {{-- <script>
        window.route_mass_crud_entries_destroy = '{{ route('stages.mass_destroy') }}';
    </script>--}}
@endsection