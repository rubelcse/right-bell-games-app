@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.questionLevel.details')</h3>
    {!! Form::open(['method' => 'POST', 'route' => ['questionLevelDetail.store']]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.create')
        </div>
        
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('question_level_id', 'Question Level*', ['class' => 'control-label']) !!}
                    {!! Form::select('question_level_id', $levels, old('question_level_id'), ['class' => 'form-control']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('question_level_id'))
                        <p class="help-block">
                            {{ $errors->first('question_level_id') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6 form-group">
                    {!! Form::label('section_a_no_of_question', 'No Of Question Of Section-A*', ['class' => 'control-label']) !!}
                    {!! Form::number('section_a_no_of_question', old('section_a_no_of_question'), ['class' => 'form-control', 'placeholder' => 'Enter Section-A Question Quantity']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('section_a_no_of_question'))
                        <p class="help-block">
                            {{ $errors->first('section_a_no_of_question') }}
                        </p>
                    @endif
                </div>
                <div class="col-xs-6 form-group">
                    {!! Form::label('section_a_no_of_point', 'Total Point Of Section-A*', ['class' => 'control-label']) !!}
                    {!! Form::number('section_a_no_of_point', old('section_a_no_of_point'), ['class' => 'form-control', 'placeholder' => 'Enter Section-A Point']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('section_a_no_of_point'))
                        <p class="help-block">
                            {{ $errors->first('section_a_no_of_point') }}
                        </p>
                    @endif
                </div>
                <div class="col-xs-6 form-group">
                    {!! Form::label('section_b_no_of_question', 'No Of Question Of Section-B*', ['class' => 'control-label']) !!}
                    {!! Form::number('section_b_no_of_question', old('section_b_no_of_question'), ['class' => 'form-control', 'placeholder' => 'Enter Section-B Question Quantity']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('section_b_no_of_question'))
                        <p class="help-block">
                            {{ $errors->first('section_b_no_of_question') }}
                        </p>
                    @endif
                </div>
                <div class="col-xs-6 form-group">
                    {!! Form::label('section_b_no_of_point', 'Total Point Of Section-B*', ['class' => 'control-label']) !!}
                    {!! Form::number('section_b_no_of_point', old('section_b_no_of_point'), ['class' => 'form-control', 'placeholder' => 'Enter Section-B Point']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('section_b_no_of_point'))
                        <p class="help-block">
                            {{ $errors->first('section_b_no_of_point') }}
                        </p>
                    @endif
                </div>
                <div class="col-xs-6 form-group">
                    {!! Form::label('section_c_no_of_question', 'No Of Question Of Section-C*', ['class' => 'control-label']) !!}
                    {!! Form::number('section_c_no_of_question', old('section_c_no_of_question'), ['class' => 'form-control', 'placeholder' => 'Enter Section-C Question Quantity']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('section_c_no_of_question'))
                        <p class="help-block">
                            {{ $errors->first('section_c_no_of_question') }}
                        </p>
                    @endif
                </div>
                <div class="col-xs-6 form-group">
                    {!! Form::label('section_c_no_of_point', 'Total Point Of Section-C*', ['class' => 'control-label']) !!}
                    {!! Form::number('section_c_no_of_point', old('section_c_no_of_point'), ['class' => 'form-control', 'placeholder' => 'Enter Section-C Point']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('section_c_no_of_point'))
                        <p class="help-block">
                            {{ $errors->first('section_c_no_of_point') }}
                        </p>
                    @endif
                </div>
            </div>
        </div>
    </div>

    {!! Form::submit(trans('quickadmin.save'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

