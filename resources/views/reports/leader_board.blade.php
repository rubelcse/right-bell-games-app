@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.reports.leader_board')</h3>

    <div class="panel panel-default">
        <div class="panel-body">
            {!! Form::open(['method' => 'POST', 'route' => ['reports.leader_board.result']]) !!}
            <div class="row">
                <div class="col-xs-3 form-group">
                    {!! Form::label('from', 'From*', ['class' => 'control-label']) !!}
                    <div class='input-group date date-timepicker'>
                        <input type='text' name="from" value="{{isset($from) ? $from : old('from')}}" class="form-control"/>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span>
                        </span>
                    </div>
                </div>
                <div class="col-xs-3 form-group">
                    {!! Form::label('to', 'To*', ['class' => 'control-label']) !!}
                    <div class='input-group date date-timepicker'>
                        <input name="to" value="{{isset($to) ? $to : old('to')}}" type='text' class="form-control"/>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span>
                        </span>
                    </div>
                </div>
                <div class="col-xs-1 form-group">
                    <label for="submit">&nbsp;</label>
                    {!! Form::submit(trans('quickadmin.Search'), ['class' => 'btn btn-danger btn-block']) !!}
                </div>
            </div>

            {!! Form::close() !!}
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.list')
        </div>

        <div class="panel-body">
            <table class="table table-bordered table-striped datatable">
                <thead>
                <tr>
                    <th>SL</th>
                    <th>Name</th>
                    <th>Phone</th>
                    <th>Refer Code</th>
                    <th>Score</th>
                    <th>Profile Image</th>
                </tr>
                </thead>

                <tbody>
                @isset($allScorers)
                    @forelse($allScorers as $key => $scorer)
                        <tr>
                            <td>{{ ++$key }}</td>
                            <td>{{ $scorer->name }}</td>
                            <td>{{ $scorer->phone_no }}</td>
                            <td>{{ $scorer->refer_code }}</td>
                            <td>{{ $scorer->score }}</td>
                            <td>
                                @if(!empty($scorer->profile_image))
                                    <img style="border-radius: 50% !important;" src="{{asset($scorer->profile_image)}}"
                                         width="65px" height="65px" alt="Profile Image">
                                @endif
                            </td>
                        </tr>
                    @empty
                        <p>No Data Found</p>
                    @endforelse

                @endisset

                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript')
    <script type="text/javascript">
        $(function () {
            $('.date-timepicker').datetimepicker({
                format: 'YYYY-MM-DD'
            })
        });
    </script>
@endsection