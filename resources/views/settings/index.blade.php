@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.settings.Settings')</h3>

   {{-- <p>
        <a href="{{ route('packages.create') }}" class="btn btn-success">@lang('quickadmin.add_new')</a>
    </p>--}}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.list')
        </div>
        <div class="panel-body">
            <table class="table table-bordered table-striped {{ count($settings) > 0 ? 'datatable' : '' }}">
                <thead>
                    <tr>
                        <th style="text-align:center;">Sr</th>
                        <th>Min Point of Membership</th>
                        <th>No. Of User Level</th>
                        <th>Quantity Of First Level User</th>
                        <th>Min. Refer Of First Level User</th>
                        <th>User Level Increase BY</th>
                        <th>Action</th>
                    </tr>
                </thead>
                
                <tbody>
                    @if (count($settings) > 0)
                        @foreach ($settings as $key => $setting)
                            <tr data-entry-id="{{ $setting->id }}">
                                <td>{{++$key}}</td>
                                <td>{{ $setting->membership_point_needed }}</td>
                                <td>{{ $setting->number_of_level }}</td>
                                <td>{{ $setting->first_level_member }}</td>
                                <td>{{ $setting->first_level_member_min_refer }}</td>
                                <td>{{ $setting->level_member_increase_by }}</td>
                                <td>
                                    <a href="{{ route('settings.edit',[$setting->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.edit')</a>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="3">@lang('quickadmin.no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript')
   {{-- <script>
        window.route_mass_crud_entries_destroy = '{{ route('stages.mass_destroy') }}';
    </script>--}}
@endsection