@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.packages.packages')</h3>
    
    {!! Form::model($setting, ['method' => 'PUT', 'route' => ['settings.update', $setting->id]]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.edit')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-xs-6 form-group">
                    {!! Form::label('membership_point_needed', 'Min Point of Membership*', ['class' => 'control-label']) !!}
                    {!! Form::number('membership_point_needed', old('membership_point_needed'), ['class' => 'form-control', 'placeholder' => 'Min Point of Membership']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('membership_point_needed'))
                        <p class="help-block">
                            {{ $errors->first('membership_point_needed') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6 form-group">
                    {!! Form::label('number_of_level', 'Number Of User Level*', ['class' => 'control-label']) !!}
                    {!! Form::number('number_of_level', old('number_of_level'), ['class' => 'form-control', 'placeholder' => 'No. Of User Level']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('number_of_level'))
                        <p class="help-block">
                            {{ $errors->first('number_of_level') }}
                        </p>
                    @endif
                </div>
                <div class="col-xs-6 form-group">
                    {!! Form::label('first_level_member', 'Quantity Of First Level*', ['class' => 'control-label']) !!}
                    {!! Form::number('first_level_member', old('first_level_member'), ['class' => 'form-control', 'placeholder' => 'Quantity Of First Level']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('first_level_member'))
                        <p class="help-block">
                            {{ $errors->first('first_level_member') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6 form-group">
                    {!! Form::label('first_level_member_min_refer', 'Minimum Refer Of First Level User*', ['class' => 'control-label']) !!}
                    {!! Form::number('first_level_member_min_refer', old('first_level_member_min_refer'), ['class' => 'form-control', 'placeholder' => 'Minimum Refer Of First Level User']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('first_level_member_min_refer'))
                        <p class="help-block">
                            {{ $errors->first('first_level_member_min_refer') }}
                        </p>
                    @endif
                </div>
                <div class="col-xs-6 form-group">
                    {!! Form::label('level_member_increase_by', 'User Level Increase BY*', ['class' => 'control-label']) !!}
                    {!! Form::number('level_member_increase_by', old('level_member_increase_by'), ['class' => 'form-control', 'placeholder' => 'User Level Increase BY']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('level_member_increase_by'))
                        <p class="help-block">
                            {{ $errors->first('level_member_increase_by') }}
                        </p>
                    @endif
                </div>
            </div>
        </div>
    </div>

    {!! Form::submit(trans('quickadmin.update'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

