@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.puzzles.stages')</h3>

    <p>
        <a href="{{ route('stages.create') }}" class="btn btn-success">@lang('quickadmin.add_new')</a>
    </p>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.list')
        </div>

        <div class="panel-body">
            <table class="table table-bordered table-striped {{ count($stages) > 0 ? 'datatable' : '' }}">
                <thead>
                    <tr>
                        <th style="text-align:center;">Sr</th>
                        <th>Stage</th>
                        <th>No of Row</th>
                        <th>No of Column</th>
                        <th>Action</th>
                    </tr>
                </thead>
                
                <tbody>
                    @if (count($stages) > 0)
                        @foreach ($stages as $key => $stage)
                            <tr data-entry-id="{{ $stage->id }}">
                                <td>{{++$key}}</td>
                                <td>{{ $stage->name }}</td>
                                <td>{{ $stage->row }}</td>
                                <td>{{ $stage->column }}</td>
                                <td>
                                    <a href="{{ route('stages.show',[$stage->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.view')</a>
                                    <a href="{{ route('stages.edit',[$stage->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.edit')</a>
                                   {{-- {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.are_you_sure")."');",
                                        'route' => ['stages.destroy', $stage->id])) !!}
                                    {!! Form::submit(trans('quickadmin.delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}--}}
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="3">@lang('quickadmin.no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript')
   {{-- <script>
        window.route_mass_crud_entries_destroy = '{{ route('stages.mass_destroy') }}';
    </script>--}}
@endsection