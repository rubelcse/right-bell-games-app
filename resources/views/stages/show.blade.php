@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.stages.stages')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.view')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>Stage</th>
                            <td>{{ $stage->name }}</td>
                        </tr>
                        <tr>
                            <th>No of Row</th>
                            <td>{{ $stage->row }}</td>
                        </tr>
                        <tr>
                            <th>No of Column</th>
                            <td>{{ $stage->column }}</td>
                        </tr>
                    </table>
                </div>
            </div>

            <p>&nbsp;</p>

            <a href="{{ route('stages.index') }}" class="btn btn-default">@lang('quickadmin.back_to_list')</a>
        </div>
    </div>
@stop