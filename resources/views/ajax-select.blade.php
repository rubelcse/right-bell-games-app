<option value="">- Select level -</option>
@if(!empty($collections))
    @foreach($collections as $key => $value)
        <option value="{{ $value->id.':'. $value->level}}">{{ $value->name }}</option>
    @endforeach
@endif