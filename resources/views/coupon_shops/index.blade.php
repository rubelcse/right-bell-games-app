@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.shop.shop')</h3>

    {{-- <p>
         <a href="{{ route('levels.create') }}" class="btn btn-success">@lang('quickadmin.add_new')</a>
     </p>--}}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.list')
        </div>

        <div class="panel-body">
            {!! Form::open(['method' => 'GET', 'route' => ['shops.index']]) !!}
            <div class="row">
                <div class="col-xs-4 form-group">
                    {!! Form::select('agent_id', $agents, isset($agent_id) ? $agent_id : old('agent_id'), ['class' => 'form-control']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('agent_id'))
                        <p class="help-block">
                            {{ $errors->first('agent_id') }}
                        </p>
                    @endif
                </div>
                <div class="col-xs-2 form-group">
                    {!! Form::submit(trans('quickadmin.Search'), ['class' => 'btn btn-danger']) !!}
                </div>
            </div>
            {!! Form::close() !!}
            <table class="table table-bordered table-striped {{ count($items) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                    <th style="text-align:center;">Sr</th>
                    <th>User Name</th>
                    <th>User Phone No</th>
                    <th>Agent</th>
                    <th>Agent Phone No</th>
                    <th class="text-center">Coupon</th>
                    <th class="text-center">Is Recive By Admin</th>
                    <th class="text-center">Coupon Point</th>
                    <th class="text-center">Coupon Price</th>
                    <th class="text-center">Action</th>
                </tr>
                </thead>

                <tbody>
                @php
                    $grandTotal = 0;
                @endphp
                @if (count($items) > 0)
                    @foreach ($items as $key => $item)
                        @php
                        //dd($item);
                            $grandTotal += $item->userCoupon->amount;
                        @endphp

                        <tr data-entry-id="{{ $item->id }}">
                            <td>{{++$key}}</td>
                            <td>{{ $item->user->name }}</td>
                            <td>{{ $item->user->phone_no }}</td>
                            <td>{{ $item->agent->name }}</td>
                            <td>{{ $item->agent->phone_no }}</td>
                            <td class="text-center success">{{ $item->userCoupon->coupon->name}}</td>
                            <td class="text-center">
                                @if($item->is_receive == 1)
                                    <span class="badge badge-success">Recive</span>
                                    @else
                                    <span class="badge badge-danger">Not Recive</span>
                                @endif
                            </td>
                            <td class="text-center info">{{$item->userCoupon->point}}</td>
                            <td class="warning text-right">{{ $item->userCoupon->amount}}</td>
                            <td class="text-center">
                                @if($item->is_receive != 1)
                                    <a href="{{ route('shops.edit',[$item->id]) }}" class="btn btn-xs btn-primary">Mark As Recive</a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="3">@lang('quickadmin.no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
                <tbody>
                <tr>
                    <td style="font-weight: bold" class="text-center alert-danger" colspan="8">Grand Total</td>
                    <td style="font-weight: bold" class="warning text-right">{{$grandTotal}}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript')
    {{-- <script>
         window.route_mass_crud_entries_destroy = '{{ route('stages.mass_destroy') }}';
     </script>--}}
@endsection