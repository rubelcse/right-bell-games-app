@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.coupon.coupon')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.view')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>Name</th>
                            <td>{{ $coupon->name }}</td>
                        </tr>
                        <tr>
                            <th>Need Point</th>
                            <td>{{ $coupon->need_point }}</td>
                        </tr>
                        <tr>
                            <th>Amount</th>
                            <td>{{ $coupon->amount }}</td>
                        </tr>
                    </table>
                </div>
            </div>

            <p>&nbsp;</p>

            <a href="{{ route('coupons.index') }}" class="btn btn-default">@lang('quickadmin.back_to_list')</a>
        </div>
    </div>
@stop