@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.apk.manage_apk')</h3>

    <p>
        <a href="{{ route('apk-manage.create') }}" class="btn btn-success">@lang('quickadmin.add_new')</a>
    </p>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.list')
        </div>

        <div class="panel-body">
            <table class="table table-bordered table-striped {{ count($items) > 0 ? 'datatable' : '' }}">
                <thead>
                <tr>
                    <th style="text-align:center;">Sr</th>
                    <th>APK Version</th>
                    <th>Release Note</th>
                    <th>Current Version</th>
                    <th>Need To Force</th>
                    <th>Action</th>
                </tr>
                </thead>

                <tbody>
                @if (count($items) > 0)
                    @foreach ($items as $key => $item)
                        <tr data-entry-id="{{ $item->id }}">
                            <td>{{++$key}}</td>
                            <td>{{ $item->version }}</td>
                            <td>{{ $item->short_description }}</td>
                            <td>
                                @if($item->is_current_version == 1)
                                    <label class="success">Yes</label>
                                @else
                                    <label class="danger">No</label>
                                @endif
                            </td>
                            <td>
                                @if($item->need_to_force == 1)
                                    <label class="success">Yes</label>
                                @else
                                    <label class="danger">No</label>
                                @endif
                            </td>

                            <td>
                                @if($item->is_current_version == 1)
                                    <a href="{{ route('apk-manage.edit',[$item->id]) }}"
                                       class="btn btn-xs btn-info">@lang('quickadmin.edit')</a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="3">@lang('quickadmin.no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript')
    {{-- <script>
         window.route_mass_crud_entries_destroy = '{{ route('stages.mass_destroy') }}';
     </script>--}}
@endsection