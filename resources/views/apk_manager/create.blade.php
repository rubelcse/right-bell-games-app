@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.apk.manage_apk')</h3>
    {!! Form::open(['method' => 'POST', 'route' => ['apk-manage.store']]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.create')
        </div>
        
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('version', 'Version*', ['class' => 'control-label']) !!}
                    {!! Form::text('version', old('version'), ['class' => 'form-control', 'placeholder' => 'Enter Version Name']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('version'))
                        <p class="help-block">
                            {{ $errors->first('version') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('short_description', 'Release Note*', ['class' => 'control-label']) !!}
                    {!! Form::text('short_description', old('short_description'), ['class' => 'form-control', 'placeholder' => 'Enter Release Note']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('short_description'))
                        <p class="help-block">
                            {{ $errors->first('short_description') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6 form-group">
                    {!! Form::label('is_current_version', 'Is Current Version*', ['class' => 'control-label']) !!}
                    {!! Form::select('is_current_version', $conditionalValue, old('is_current_version'), ['class' => 'form-control']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('is_current_version'))
                        <p class="help-block">
                            {{ $errors->first('is_current_version') }}
                        </p>
                    @endif
                </div>
                <div class="col-xs-6 form-group">
                    {!! Form::label('need_to_force', 'Need To Force Update*', ['class' => 'control-label']) !!}
                    {!! Form::select('need_to_force', $conditionalValue, old('need_to_force'), ['class' => 'form-control']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('need_to_force'))
                        <p class="help-block">
                            {{ $errors->first('need_to_force') }}
                        </p>
                    @endif
                </div>
            </div>
        </div>
    </div>

    {!! Form::submit(trans('quickadmin.save'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

